/* poppynotes@perichron.org +++ 2013-07-07
 * DBus client to control the indicator externally.
 * Used to show and hide all notes.
 * (To set an accelerator (keyboard shortcut) use the system's
 *  System Settings -> Keyboard -> Shortcuts)
 */
namespace PoppyNotes {

    [DBus (name = "org.perichron.PoppyNotes")]
    interface PoppyNotesDBus : Object {
        public abstract void show_hide_all_notes_toggle() throws IOError;
        public abstract void show_all_notes() throws IOError;
        public abstract void hide_all_notes() throws IOError;
        public abstract void new_note() throws IOError;
        public abstract void show_settings() throws IOError;
    }

    public class DBusClient {

        public static int main(string[] args) {
            if (args.length != 2) {
                stderr.printf("The PoppyNotes DBusClient needs one parameter, giving the action to take!\n");
                stderr.printf("%s ACTION\n", args[0]);
                stderr.printf("    ACTION: %s | %s | %s | %s | %s\n", DBusUtil.DBUS_ACTION_SHOWHIDE,
                              DBusUtil.DBUS_ACTION_SHOW, DBusUtil.DBUS_ACTION_HIDE,
                              DBusUtil.DBUS_ACTION_NEWNOTE, DBusUtil.DBUS_ACTION_SETTINGS);
                return 0;
            }
            PoppyNotesDBus bus = null;
            try {
                bus = Bus.get_proxy_sync(BusType.SESSION,
                                         "org.perichron.PoppyNotes",
                                         "/org/perichron/poppynotes");
                switch (args[1]) {
                    case DBusUtil.DBUS_ACTION_SHOWHIDE:
                        bus.show_hide_all_notes_toggle();
                        break;
                    case DBusUtil.DBUS_ACTION_SHOW:
                        bus.show_all_notes();
                        break;
                    case DBusUtil.DBUS_ACTION_HIDE:
                        bus.hide_all_notes();
                        break;
                    case DBusUtil.DBUS_ACTION_NEWNOTE:
                        bus.new_note();
                        break;
                    case DBusUtil.DBUS_ACTION_SETTINGS:
                        bus.show_settings();
                        break;
                    default:
                        stderr.printf("The given action (%s) is not supported!\n", args[1]);
                        break;
                }
            } catch (IOError e) {
                stderr.printf ("%s\n", e.message);
            }

            return 0;
        }
    }
}
