/* poppynotes@perichron.org +++ 2013-06-25
 * Class with a class method to provide an instance of a note loader,
 * that should be used throughout the application.
 */
namespace PoppyNotes {
    public class NoteLoaderProvider {
        protected static INoteLoader note_loader;
        /*
         * Returns the note loader to use throughout the application.
         */
        public static INoteLoader get_note_loader() {
            if (note_loader == null)
                note_loader = new NoteLoader();
            return note_loader;
        }
    }
}