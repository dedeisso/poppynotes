/* poppynotes@perichron.org +++ 2013-06-25
 * Display settings and let the user edit them.
 */
using Gtk;

namespace PoppyNotes {
    public class SettingsDialog : Gtk.Dialog {

//////////////////
//  Properties  //
//////////////////

        public string notes_directory {
            owned get { return this.filechooserbutton_notes_directory.get_current_folder_uri().replace("file://", ""); }
            set { this.filechooserbutton_notes_directory.set_current_folder_uri("file://" + value.replace("file://", "")); }
        }
        public string default_content_type {
            get { return this.entry_default_content_type.text; }
            set { this.entry_default_content_type.text = value; }
        }
        public bool start_with_system {
            get { return this.checkbutton_start_with_system.active; }
            set { this.checkbutton_start_with_system.active = value; }
        }
        public bool show_on_startup {
            get { return this.checkbutton_show_on_startup.active; }
            set { this.checkbutton_show_on_startup.active = value; }
        }
        /*
         * Set the poppy notes setting 'default_width'.
         */
		public int default_width_pn {
            get { return int.parse(this.entry_default_width.text); }
            set { this.entry_default_width.text = value.to_string(); }
        }
        /*
         * Set the poppy notes setting 'default_height'.
         */
		public int default_height_pn {
            get { return int.parse(this.entry_default_height.text); }
            set { this.entry_default_height.text = value.to_string(); }
        }
		public int default_pos_x {
            get { return int.parse(this.entry_default_pos_x.text); }
            set { this.entry_default_pos_x.text = value.to_string(); }
        }
		public int default_pos_y {
            get { return int.parse(this.entry_default_pos_y.text); }
            set { this.entry_default_pos_y.text = value.to_string(); }
        }
        public string default_font_family {
            owned get { return Util.font_to_family(this.fontbutton_default_font.font_name); }
            set { this.fontbutton_default_font.font_name = value + " " + this.default_font_size.to_string(); }
        }
        public float default_font_size {
            get { return Util.font_to_size(this.fontbutton_default_font.font_name); }
            set { this.fontbutton_default_font.font_name = this.default_font_family + " " + value.to_string(); }
        }
        public string default_font_color {
            owned get { return this.colorbutton_default_font_color.rgba.to_string(); }
            set { var col = Gdk.RGBA(); col.parse(value); this.colorbutton_default_font_color.rgba = col; }
        }
        public string default_bg_color {
            owned get { return this.colorbutton_default_bg_color.rgba.to_string(); }
            set { var col = Gdk.RGBA(); col.parse(value); this.colorbutton_default_bg_color.rgba = col; }
        }
        public ShortcutKey shortcut_showhide {
            get {
                return ShortcutKey() {
                    enabled = this.checkbutton_enable_shortcut_showhide.active,
                    ctrl = this.togglebutton_ctrl_showhide.active,
                    alt = this.togglebutton_alt_showhide.active,
                    shift = this.togglebutton_shift_showhide.active,
                    super = this.togglebutton_super_showhide.active,
                    charnum = this.entry_key_showhide.text.get(0)
                };
            }
            set {
                this.checkbutton_enable_shortcut_showhide.active = value.enabled;
                this.togglebutton_ctrl_showhide.active = value.ctrl;
                this.togglebutton_alt_showhide.active = value.alt;
                this.togglebutton_shift_showhide.active = value.shift;
                this.togglebutton_super_showhide.active = value.super;
                this.entry_key_showhide.text = value.charnum.to_string();
            }
        }
        public ShortcutKey shortcut_show {
            get {
                return ShortcutKey() {
                    enabled = this.checkbutton_enable_shortcut_show.active,
                    ctrl = this.togglebutton_ctrl_show.active,
                    alt = this.togglebutton_alt_show.active,
                    shift = this.togglebutton_shift_show.active,
                    super = this.togglebutton_super_show.active,
                    charnum = this.entry_key_show.text.get(0)
                };
            }
            set {
                this.checkbutton_enable_shortcut_show.active = value.enabled;
                this.togglebutton_ctrl_show.active = value.ctrl;
                this.togglebutton_alt_show.active = value.alt;
                this.togglebutton_shift_show.active = value.shift;
                this.togglebutton_super_show.active = value.super;
                this.entry_key_show.text = value.charnum.to_string();
            }
        }
        public ShortcutKey shortcut_hide {
            get {
                return ShortcutKey() {
                    enabled = this.checkbutton_enable_shortcut_hide.active,
                    ctrl = this.togglebutton_ctrl_hide.active,
                    alt = this.togglebutton_alt_hide.active,
                    shift = this.togglebutton_shift_hide.active,
                    super = this.togglebutton_super_hide.active,
                    charnum = this.entry_key_hide.text.get(0)
                };
            }
            set {
                this.checkbutton_enable_shortcut_hide.active = value.enabled;
                this.togglebutton_ctrl_hide.active = value.ctrl;
                this.togglebutton_alt_hide.active = value.alt;
                this.togglebutton_shift_hide.active = value.shift;
                this.togglebutton_super_hide.active = value.super;
                this.entry_key_hide.text = value.charnum.to_string();
            }
        }
        public ShortcutKey shortcut_new {
            get {
                return ShortcutKey() {
                    enabled = this.checkbutton_enable_shortcut_new.active,
                    ctrl = this.togglebutton_ctrl_new.active,
                    alt = this.togglebutton_alt_new.active,
                    shift = this.togglebutton_shift_new.active,
                    super = this.togglebutton_super_new.active,
                    charnum = this.entry_key_new.text.get(0)
                };
            }
            set {
                this.checkbutton_enable_shortcut_new.active = value.enabled;
                this.togglebutton_ctrl_new.active = value.ctrl;
                this.togglebutton_alt_new.active = value.alt;
                this.togglebutton_shift_new.active = value.shift;
                this.togglebutton_super_new.active = value.super;
                this.entry_key_new.text = value.charnum.to_string();
            }
        }
        public ShortcutKey shortcut_sett {
            get {
                return ShortcutKey() {
                    enabled = this.checkbutton_enable_shortcut_sett.active,
                    ctrl = this.togglebutton_ctrl_sett.active,
                    alt = this.togglebutton_alt_sett.active,
                    shift = this.togglebutton_shift_sett.active,
                    super = this.togglebutton_super_sett.active,
                    charnum = this.entry_key_sett.text.get(0)
                };
            }
            set {
                this.checkbutton_enable_shortcut_sett.active = value.enabled;
                this.togglebutton_ctrl_sett.active = value.ctrl;
                this.togglebutton_alt_sett.active = value.alt;
                this.togglebutton_shift_sett.active = value.shift;
                this.togglebutton_super_sett.active = value.super;
                this.entry_key_sett.text = value.charnum.to_string();
            }
        }

        public void set_all_settings(Settings settings) {
            this.notes_directory = settings.notes_directory;
            this.default_width_pn = settings.default_width;
            this.default_height_pn = settings.default_height;
            this.default_pos_x = settings.default_pos_x;
            this.default_pos_y = settings.default_pos_y;
            this.default_font_family = settings.default_font_family;
            this.default_font_size = settings.default_font_size;
            this.default_font_color = settings.default_font_color;
            this.default_bg_color = settings.default_bg_color;
            this.default_content_type = settings.default_content_type;
            this.shortcut_showhide = settings.shortcut_showhide;
            this.shortcut_show = settings.shortcut_show;
            this.shortcut_hide = settings.shortcut_hide;
            this.shortcut_new = settings.shortcut_new;
            this.shortcut_sett = settings.shortcut_sett;
            this.show_on_startup = settings.show_on_startup;
            this.start_with_system = settings.start_with_system;
        }

////////////
//  INIT  //
////////////

        public void init() {
            this.entry_key_showhide.changed.connect(this._check_shortcut_key_is_alpha);
            this.entry_key_show.changed.connect(this._check_shortcut_key_is_alpha);
            this.entry_key_hide.changed.connect(this._check_shortcut_key_is_alpha);
            this.entry_key_new.changed.connect(this._check_shortcut_key_is_alpha);
            this.entry_key_sett.changed.connect(this._check_shortcut_key_is_alpha);
        }

        private void _check_shortcut_key_is_alpha(Gtk.Editable source) {
            if ((source as Gtk.Entry).text != "" && !(source as Gtk.Entry).text.get(0).isalpha()) {
                var d = new Gtk.MessageDialog(this, Gtk.DialogFlags.MODAL, Gtk.MessageType.ERROR, Gtk.ButtonsType.OK,
                                              _("The key of the shortcut has to be a character between 'a' and 'z'."));
                d.title = _("Error");
                d.response.connect((response_id) => { d.destroy(); });
                d.show();
            }
        }

////////////////////
//  GUI-Elements  //
////////////////////

////  General  ////

        private Gtk.FileChooserButton _filechooserbutton_notes_directory;
        protected Gtk.FileChooserButton filechooserbutton_notes_directory {
            get {
                if (this._filechooserbutton_notes_directory == null)
                    this._filechooserbutton_notes_directory = Util.get_child_by_name(this, "filechooserbutton_notes_directory") as Gtk.FileChooserButton;
                return this._filechooserbutton_notes_directory;
            }
        }
        private Gtk.Entry _entry_default_content_type;
        protected Gtk.Entry entry_default_content_type {
            get {
                if (this._entry_default_content_type == null)
                    this._entry_default_content_type = Util.get_child_by_name(this, "entry_default_content_type") as Gtk.Entry;
                return this._entry_default_content_type;
            }
        }
        private Gtk.CheckButton _checkbutton_start_with_system;
        protected Gtk.CheckButton checkbutton_start_with_system {
            get {
                if (this._checkbutton_start_with_system == null)
                    this._checkbutton_start_with_system = Util.get_child_by_name(this, "checkbutton_start_with_system") as Gtk.CheckButton;
                return this._checkbutton_start_with_system;
            }
        }
        private Gtk.CheckButton _checkbutton_show_on_startup;
        protected Gtk.CheckButton checkbutton_show_on_startup {
            get {
                if (this._checkbutton_show_on_startup == null)
                    this._checkbutton_show_on_startup = Util.get_child_by_name(this, "checkbutton_show_on_startup") as Gtk.CheckButton;
                return this._checkbutton_show_on_startup;
            }
        }

////  Appearance  ////

        private Gtk.Entry _entry_default_width;
        protected Gtk.Entry entry_default_width {
            get {
                if (this._entry_default_width == null)
                    this._entry_default_width = Util.get_child_by_name(this, "entry_default_width") as Gtk.Entry;
                return this._entry_default_width;
            }
        }
        private Gtk.Entry _entry_default_height;
        protected Gtk.Entry entry_default_height {
            get {
                if (this._entry_default_height == null)
                    this._entry_default_height = Util.get_child_by_name(this, "entry_default_height") as Gtk.Entry;
                return this._entry_default_height;
            }
        }
        private Gtk.Entry _entry_default_pos_x;
        protected Gtk.Entry entry_default_pos_x {
            get {
                if (this._entry_default_pos_x == null)
                    this._entry_default_pos_x = Util.get_child_by_name(this, "entry_default_pos_x") as Gtk.Entry;
                return this._entry_default_pos_x;
            }
        }
        private Gtk.Entry _entry_default_pos_y;
        protected Gtk.Entry entry_default_pos_y {
            get {
                if (this._entry_default_pos_y == null)
                    this._entry_default_pos_y = Util.get_child_by_name(this, "entry_default_pos_y") as Gtk.Entry;
                return this._entry_default_pos_y;
            }
        }
        private Gtk.FontButton _fontbutton_default_font;
        protected Gtk.FontButton fontbutton_default_font {
            get {
                if (this._fontbutton_default_font == null)
                    this._fontbutton_default_font = Util.get_child_by_name(this, "fontbutton_default_font") as Gtk.FontButton;
                return this._fontbutton_default_font;
            }
        }
        private Gtk.ColorButton _colorbutton_default_font_color;
        protected Gtk.ColorButton colorbutton_default_font_color {
            get {
                if (this._colorbutton_default_font_color == null)
                    this._colorbutton_default_font_color = Util.get_child_by_name(this, "colorbutton_default_font_color") as Gtk.ColorButton;
                return this._colorbutton_default_font_color;
            }
        }
        private Gtk.ColorButton _colorbutton_default_bg_color;
        protected Gtk.ColorButton colorbutton_default_bg_color {
            get {
                if (this._colorbutton_default_bg_color == null)
                    this._colorbutton_default_bg_color = Util.get_child_by_name(this, "colorbutton_default_bg_color") as Gtk.ColorButton;
                return this._colorbutton_default_bg_color;
            }
        }

////  Shortcuts  ////

        private Gtk.CheckButton _checkbutton_enable_shortcut_showhide;
        public Gtk.CheckButton checkbutton_enable_shortcut_showhide {
            get {
                if (this._checkbutton_enable_shortcut_showhide == null)
                    this._checkbutton_enable_shortcut_showhide = Util.get_child_by_name(this, "checkbutton_enable_shortcut_showhide") as Gtk.CheckButton;
                return this._checkbutton_enable_shortcut_showhide;
            }
        }
        private Gtk.CheckButton _checkbutton_enable_shortcut_show;
        public Gtk.CheckButton checkbutton_enable_shortcut_show {
            get {
                if (this._checkbutton_enable_shortcut_show == null)
                    this._checkbutton_enable_shortcut_show = Util.get_child_by_name(this, "checkbutton_enable_shortcut_show") as Gtk.CheckButton;
                return this._checkbutton_enable_shortcut_show;
            }
        }
        private Gtk.CheckButton _checkbutton_enable_shortcut_hide;
        public Gtk.CheckButton checkbutton_enable_shortcut_hide {
            get {
                if (this._checkbutton_enable_shortcut_hide == null)
                    this._checkbutton_enable_shortcut_hide = Util.get_child_by_name(this, "checkbutton_enable_shortcut_hide") as Gtk.CheckButton;
                return this._checkbutton_enable_shortcut_hide;
            }
        }
        private Gtk.CheckButton _checkbutton_enable_shortcut_new;
        public Gtk.CheckButton checkbutton_enable_shortcut_new {
            get {
                if (this._checkbutton_enable_shortcut_new == null)
                    this._checkbutton_enable_shortcut_new = Util.get_child_by_name(this, "checkbutton_enable_shortcut_new") as Gtk.CheckButton;
                return this._checkbutton_enable_shortcut_new;
            }
        }
        private Gtk.CheckButton _checkbutton_enable_shortcut_sett;
        public Gtk.CheckButton checkbutton_enable_shortcut_sett {
            get {
                if (this._checkbutton_enable_shortcut_sett == null)
                    this._checkbutton_enable_shortcut_sett = Util.get_child_by_name(this, "checkbutton_enable_shortcut_sett") as Gtk.CheckButton;
                return this._checkbutton_enable_shortcut_sett;
            }
        }

        private Gtk.ToggleButton _togglebutton_ctrl_showhide;
        public Gtk.ToggleButton togglebutton_ctrl_showhide {
            get {
                if (this._togglebutton_ctrl_showhide == null)
                    this._togglebutton_ctrl_showhide = Util.get_child_by_name(this, "togglebutton_ctrl_showhide") as Gtk.ToggleButton;
                return this._togglebutton_ctrl_showhide;
            }
        }
        private Gtk.ToggleButton _togglebutton_ctrl_show;
        public Gtk.ToggleButton togglebutton_ctrl_show {
            get {
                if (this._togglebutton_ctrl_show == null)
                    this._togglebutton_ctrl_show = Util.get_child_by_name(this, "togglebutton_ctrl_show") as Gtk.ToggleButton;
                return this._togglebutton_ctrl_show;
            }
        }
        private Gtk.ToggleButton _togglebutton_ctrl_hide;
        public Gtk.ToggleButton togglebutton_ctrl_hide {
            get {
                if (this._togglebutton_ctrl_hide == null)
                    this._togglebutton_ctrl_hide = Util.get_child_by_name(this, "togglebutton_ctrl_hide") as Gtk.ToggleButton;
                return this._togglebutton_ctrl_hide;
            }
        }
        private Gtk.ToggleButton _togglebutton_ctrl_new;
        public Gtk.ToggleButton togglebutton_ctrl_new {
            get {
                if (this._togglebutton_ctrl_new == null)
                    this._togglebutton_ctrl_new = Util.get_child_by_name(this, "togglebutton_ctrl_new") as Gtk.ToggleButton;
                return this._togglebutton_ctrl_new;
            }
        }
        private Gtk.ToggleButton _togglebutton_ctrl_sett;
        public Gtk.ToggleButton togglebutton_ctrl_sett {
            get {
                if (this._togglebutton_ctrl_sett == null)
                    this._togglebutton_ctrl_sett = Util.get_child_by_name(this, "togglebutton_ctrl_sett") as Gtk.ToggleButton;
                return this._togglebutton_ctrl_sett;
            }
        }

        private Gtk.ToggleButton _togglebutton_alt_showhide;
        public Gtk.ToggleButton togglebutton_alt_showhide {
            get {
                if (this._togglebutton_alt_showhide == null)
                    this._togglebutton_alt_showhide = Util.get_child_by_name(this, "togglebutton_alt_showhide") as Gtk.ToggleButton;
                return this._togglebutton_alt_showhide;
            }
        }
        private Gtk.ToggleButton _togglebutton_alt_show;
        public Gtk.ToggleButton togglebutton_alt_show {
            get {
                if (this._togglebutton_alt_show == null)
                    this._togglebutton_alt_show = Util.get_child_by_name(this, "togglebutton_alt_show") as Gtk.ToggleButton;
                return this._togglebutton_alt_show;
            }
        }
        private Gtk.ToggleButton _togglebutton_alt_hide;
        public Gtk.ToggleButton togglebutton_alt_hide {
            get {
                if (this._togglebutton_alt_hide == null)
                    this._togglebutton_alt_hide = Util.get_child_by_name(this, "togglebutton_alt_hide") as Gtk.ToggleButton;
                return this._togglebutton_alt_hide;
            }
        }
        private Gtk.ToggleButton _togglebutton_alt_new;
        public Gtk.ToggleButton togglebutton_alt_new {
            get {
                if (this._togglebutton_alt_new == null)
                    this._togglebutton_alt_new = Util.get_child_by_name(this, "togglebutton_alt_new") as Gtk.ToggleButton;
                return this._togglebutton_alt_new;
            }
        }
        private Gtk.ToggleButton _togglebutton_alt_sett;
        public Gtk.ToggleButton togglebutton_alt_sett {
            get {
                if (this._togglebutton_alt_sett == null)
                    this._togglebutton_alt_sett = Util.get_child_by_name(this, "togglebutton_alt_sett") as Gtk.ToggleButton;
                return this._togglebutton_alt_sett;
            }
        }

        private Gtk.ToggleButton _togglebutton_shift_showhide;
        public Gtk.ToggleButton togglebutton_shift_showhide {
            get {
                if (this._togglebutton_shift_showhide == null)
                    this._togglebutton_shift_showhide = Util.get_child_by_name(this, "togglebutton_shift_showhide") as Gtk.ToggleButton;
                return this._togglebutton_shift_showhide;
            }
        }
        private Gtk.ToggleButton _togglebutton_shift_show;
        public Gtk.ToggleButton togglebutton_shift_show {
            get {
                if (this._togglebutton_shift_show == null)
                    this._togglebutton_shift_show = Util.get_child_by_name(this, "togglebutton_shift_show") as Gtk.ToggleButton;
                return this._togglebutton_shift_show;
            }
        }
        private Gtk.ToggleButton _togglebutton_shift_hide;
        public Gtk.ToggleButton togglebutton_shift_hide {
            get {
                if (this._togglebutton_shift_hide == null)
                    this._togglebutton_shift_hide = Util.get_child_by_name(this, "togglebutton_shift_hide") as Gtk.ToggleButton;
                return this._togglebutton_shift_hide;
            }
        }
        private Gtk.ToggleButton _togglebutton_shift_new;
        public Gtk.ToggleButton togglebutton_shift_new {
            get {
                if (this._togglebutton_shift_new == null)
                    this._togglebutton_shift_new = Util.get_child_by_name(this, "togglebutton_shift_new") as Gtk.ToggleButton;
                return this._togglebutton_shift_new;
            }
        }
        private Gtk.ToggleButton _togglebutton_shift_sett;
        public Gtk.ToggleButton togglebutton_shift_sett {
            get {
                if (this._togglebutton_shift_sett == null)
                    this._togglebutton_shift_sett = Util.get_child_by_name(this, "togglebutton_shift_sett") as Gtk.ToggleButton;
                return this._togglebutton_shift_sett;
            }
        }

        private Gtk.ToggleButton _togglebutton_super_showhide;
        public Gtk.ToggleButton togglebutton_super_showhide {
            get {
                if (this._togglebutton_super_showhide == null)
                    this._togglebutton_super_showhide = Util.get_child_by_name(this, "togglebutton_super_showhide") as Gtk.ToggleButton;
                return this._togglebutton_super_showhide;
            }
        }
        private Gtk.ToggleButton _togglebutton_super_show;
        public Gtk.ToggleButton togglebutton_super_show {
            get {
                if (this._togglebutton_super_show == null)
                    this._togglebutton_super_show = Util.get_child_by_name(this, "togglebutton_super_show") as Gtk.ToggleButton;
                return this._togglebutton_super_show;
            }
        }
        private Gtk.ToggleButton _togglebutton_super_hide;
        public Gtk.ToggleButton togglebutton_super_hide {
            get {
                if (this._togglebutton_super_hide == null)
                    this._togglebutton_super_hide = Util.get_child_by_name(this, "togglebutton_super_hide") as Gtk.ToggleButton;
                return this._togglebutton_super_hide;
            }
        }
        private Gtk.ToggleButton _togglebutton_super_new;
        public Gtk.ToggleButton togglebutton_super_new {
            get {
                if (this._togglebutton_super_new == null)
                    this._togglebutton_super_new = Util.get_child_by_name(this, "togglebutton_super_new") as Gtk.ToggleButton;
                return this._togglebutton_super_new;
            }
        }
        private Gtk.ToggleButton _togglebutton_super_sett;
        public Gtk.ToggleButton togglebutton_super_sett {
            get {
                if (this._togglebutton_super_sett == null)
                    this._togglebutton_super_sett = Util.get_child_by_name(this, "togglebutton_super_sett") as Gtk.ToggleButton;
                return this._togglebutton_super_sett;
            }
        }

        private Gtk.Entry _entry_key_showhide;
        public Gtk.Entry entry_key_showhide {
            get {
                if (this._entry_key_showhide == null)
                    this._entry_key_showhide = Util.get_child_by_name(this, "entry_key_showhide") as Gtk.Entry;
                return this._entry_key_showhide;
            }
        }
        private Gtk.Entry _entry_key_show;
        public Gtk.Entry entry_key_show {
            get {
                if (this._entry_key_show == null)
                    this._entry_key_show = Util.get_child_by_name(this, "entry_key_show") as Gtk.Entry;
                return this._entry_key_show;
            }
        }
        private Gtk.Entry _entry_key_hide;
        public Gtk.Entry entry_key_hide {
            get {
                if (this._entry_key_hide == null)
                    this._entry_key_hide = Util.get_child_by_name(this, "entry_key_hide") as Gtk.Entry;
                return this._entry_key_hide;
            }
        }
        private Gtk.Entry _entry_key_new;
        public Gtk.Entry entry_key_new {
            get {
                if (this._entry_key_new == null)
                    this._entry_key_new = Util.get_child_by_name(this, "entry_key_new") as Gtk.Entry;
                return this._entry_key_new;
            }
        }
        private Gtk.Entry _entry_key_sett;
        public Gtk.Entry entry_key_sett {
            get {
                if (this._entry_key_sett == null)
                    this._entry_key_sett = Util.get_child_by_name(this, "entry_key_sett") as Gtk.Entry;
                return this._entry_key_sett;
            }
        }

////  Buttons  ////

        private Gtk.Button _button_cancel;
        public Gtk.Button button_cancel {
            get {
                if (this._button_cancel == null)
                    this._button_cancel = Util.get_child_by_name(this, "button_cancel") as Gtk.Button;
                return this._button_cancel;
            }
        }
        private Gtk.Button _button_save;
        public Gtk.Button button_save {
            get {
                if (this._button_save == null)
                    this._button_save = Util.get_child_by_name(this, "button_save") as Gtk.Button;
                return this._button_save;
            }
        }
    }
}