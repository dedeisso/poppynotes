/* poppynotes@perichron.org +++ 2013-06-25
 * Class with a class method to provide an instance of a settings loader,
 * that should be used throughout the application.
 */
namespace PoppyNotes {
    public class SettingsLoaderProvider {
        // the following two ways do not work:
        // 1.:
        //  protected static ISettingsLoader settings_loader = new SettingsLoader();
        //
        // 2.:
        //  static construct {
        //    settings_loader = new SettingsLoader();
        //  }

        protected static ISettingsLoader settings_loader;
        /*
         * Returns the settings loader to use throughout the application.
         */
        public static ISettingsLoader get_settings_loader() {
            if (settings_loader == null)
                settings_loader = new SettingsLoader();
            return settings_loader;
        }
    }
}