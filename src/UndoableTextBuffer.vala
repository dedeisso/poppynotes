/* poppynotes@perichron.org +++ 2013-07-07
 * TextBuffer with undo and redo functionallity.
 * Implemented for Vala follwing the Python implementation at:
 * https://bitbucket.org/tiax/gtk-textbuffer-with-undo
 */
using Gtk;

namespace PoppyNotes {

    protected errordomain IndexError {
        OUT_OF_BOUNDS
    }

    /*
     * Stack for holding previous undos and redos.
     */
    protected class Stack<T> {
        private T[] _stack = new T[32];
        private int _length = 0;
        public int size { get { return this._length; } }
        public bool empty { get { return (this._length <= 0); } }
        
        public void push(T element) {
            if (this._length >= this._stack.length)
                this._stack.resize(this._stack.length * 2);
            this._stack[this._length] = element;
            this._length = this._length + 1;
        }
        public T pop() throws IndexError {
            if (this.empty) throw new IndexError.OUT_OF_BOUNDS("The stack is empty!");
            this._length--;
            return this._stack[this._length];       
        }
        public void clear() {
            this._length = 0;
        }
    }

    /*
     * Class representing undoable actions, i.e. inserts and deletes.
     */
    protected class UndoableAction : Object {
        public string text { get; set; }
        public bool mergeable { get; set; }
    }

    /*
     * Class representing undoable inserts.
     */
    protected class UndoableInsert : UndoableAction {
        public int offset { get; set; }
        public int length { get; set; }

        public UndoableInsert(TextIter text_iter, string text, int length) {
            this.offset = text_iter.get_offset();
            this.text = text;
            this.length = length;
            string[] WS = {"\r", "\n", " "};
            if (this.length > 1 || this.text in WS)
                this.mergeable = false;
            else
                this.mergeable = true;
        }
    }

    /*
     * Class representing undoable deletes.
     */
    protected class UndoableDelete : UndoableAction {
        public int start { get; set; }
        public int end { get; set; }
        public bool delete_key_used { get; set; }

        public UndoableDelete(TextBuffer text_buffer, TextIter start_iter, TextIter end_iter) {
            this.text = text_buffer.get_text(start_iter, end_iter, true);
            this.start = start_iter.get_offset();
            this.end = end_iter.get_offset();
            //# need to find out if backspace or delete key has been used
            //# so we don't mess up during redo
            TextIter insert_iter;
            text_buffer.get_iter_at_mark(out insert_iter, text_buffer.get_insert());
            if (insert_iter.get_offset() <= this.start)
                this.delete_key_used = true;
            else
                this.delete_key_used = false;
            string[] WS = {"\r", "\n", " "};
            if (this.end - this.start > 1 || this.text in WS)
                this.mergeable = false;
            else
                this.mergeable = true;
        }
    }

    /*
     * TextBuffer with Undo/Redo functionallity.
     */
    public class UndoableTextBuffer : Gtk.TextBuffer {

        private Stack<UndoableAction> undo_stack = new Stack<UndoableAction>();
        private Stack<UndoableAction> redo_stack = new Stack<UndoableAction>();
        private bool not_undoable_action = false;
        private bool undo_in_progress = false;
        
        public UndoableTextBuffer() {
            this.insert_text.connect(this.on_insert_text);
            this.delete_range.connect(this.on_delete_range);
        }

        public bool can_undo() { return (this.undo_stack.size > 0); }
        public bool can_redo() { return (this.redo_stack.size > 0); }

        /*
         * See if we can merge multiple inserts.
         * Can't merge if prev and cur are not mergeable in the first place.
         * Can't merge when user set the cursor somewhere else.
         * Can't merge across word boundaries.
         */
        private bool inserts_can_be_merged(UndoableInsert prev, UndoableInsert cur) {
            string[] WHITESPACE = {" ", "\t"};
            if (!cur.mergeable || !prev.mergeable)
                return false;
            else if (cur.offset != (prev.offset + prev.length))
                return false;
            else if (cur.text in WHITESPACE && !(prev.text in WHITESPACE))
                return false;
            else if (prev.text in WHITESPACE && !(cur.text in WHITESPACE))
                return false;
            return true;
        }

        /*
         * Gets called when text has been inserted.
         */
        public void on_insert_text(Gtk.TextBuffer textbuffer, Gtk.TextIter text_iter, string text, int length) {
            
            if (!this.undo_in_progress)
                this.redo_stack.clear();
            if (this.not_undoable_action)
                return;
            var undo_action = new UndoableInsert(text_iter, text, length);
            UndoableAction prev_action;
            try {
                prev_action = this.undo_stack.pop();
            } catch (IndexError e) {
                this.undo_stack.push(undo_action);
                return;
            }
            if (!(prev_action is UndoableInsert)) {
                this.undo_stack.push(prev_action);
                this.undo_stack.push(undo_action);
                return;
            }
            var prev_insert = prev_action as UndoableInsert;
            if (inserts_can_be_merged(prev_insert, undo_action)) {
                prev_insert.length = prev_insert.length + undo_action.length;
                prev_insert.text = prev_insert.text + undo_action.text;
                this.undo_stack.push(prev_insert);
            } else {
                this.undo_stack.push(prev_insert);
                this.undo_stack.push(undo_action);
            }
        }

        /*
         * See if we can merge multiple deletions.
         * Can't merge if prev and cur are not mergeable in the first place.
         * Can't merge if delete and backspace key were both used.
         * Can't merge across word boundaries.
         */
        private bool deletes_can_be_merged(UndoableDelete prev, UndoableDelete cur) {
            string[] WHITESPACE = {" ", "\t"};
            if (!cur.mergeable || !prev.mergeable)
                return false;
            else if (prev.delete_key_used != cur.delete_key_used)
                return false;
            else if (prev.start != cur.start && prev.start != cur.end)
                return false;
            else if (!(cur.text in WHITESPACE) && prev.text in WHITESPACE)
                return false;
            else if (cur.text in WHITESPACE && !(prev.text in WHITESPACE))
                return false;
            return true;
        }

        /*
         * Called when text has been deleted.
         */
        public void on_delete_range(TextBuffer text_buffer, TextIter start_iter, TextIter end_iter) {

            if (!this.undo_in_progress)
                this.redo_stack.clear();
            if (this.not_undoable_action)
                return;
            var undo_action = new UndoableDelete(text_buffer, start_iter, end_iter);
            UndoableAction prev_action;
            try {
                prev_action = this.undo_stack.pop();
            } catch (IndexError e) {
                this.undo_stack.push(undo_action);
                return;
            }
            if (!(prev_action is UndoableDelete)) {
                this.undo_stack.push(prev_action);
                this.undo_stack.push(undo_action);
                return;
            }
            var prev_delete = prev_action as UndoableDelete;
            if (deletes_can_be_merged(prev_delete, undo_action)) {
                if (prev_delete.start == undo_action.start) { // Delete key used
                    prev_delete.text = prev_delete.text + undo_action.text;
                    prev_delete.end = prev_delete.end + (undo_action.end - undo_action.start);
                } else { // Backspace used
                    prev_delete.text = undo_action.text + prev_delete.text;
                    prev_delete.start = undo_action.start;
                }
                this.undo_stack.push(prev_delete);
            } else {
                this.undo_stack.push(prev_delete);
                this.undo_stack.push(undo_action);
            }
        }

        /*
         * Do not record the next actions.
         */
        public void begin_not_undoable_action() {
            this.not_undoable_action = true;
        }

        /*
         * Do (again) record the next actions.
         */
        public void end_not_undoable_action() {
            this.not_undoable_action = false;
        }

        /*
         * Undo the latest action (insert or delete).
         * Undone actions get moved to the redo stack.
         */
        public void undo() {
            if (this.undo_stack.empty)
                return;
            this.begin_not_undoable_action();
            this.undo_in_progress = true;
            UndoableAction undo_action = new UndoableAction();
            try {
                undo_action = this.undo_stack.pop();
            } catch (IndexError e) { /* ignore IndexError! if empty: we already returned */ }
            this.redo_stack.push(undo_action);
            TextIter start;
            TextIter stop;
            if (undo_action is UndoableInsert) {
                this.get_iter_at_offset(out start, (undo_action as UndoableInsert).offset);
                this.get_iter_at_offset(out stop, (undo_action as UndoableInsert).offset + (undo_action as UndoableInsert).length);
                this.delete(ref start, ref stop);
                this.place_cursor(start);
            } else {
                this.get_iter_at_offset(out start, (undo_action as UndoableDelete).start);
                this.insert(ref start, undo_action.text, -1); //-1 ... entire text
                this.get_iter_at_offset(out stop, (undo_action as UndoableDelete).end);
                if ((undo_action as UndoableDelete).delete_key_used)
                    this.place_cursor(start);
                else
                    this.place_cursor(stop);
            }
            this.end_not_undoable_action();
            this.undo_in_progress = false;
        }

        /*
         * Redo a previously undone action.
         * Redone actions get moved to the undo stack.
         */
        public void redo() {
            if (this.redo_stack.empty)
                return;
            this.begin_not_undoable_action();
            this.undo_in_progress = true;
            UndoableAction redo_action = new UndoableAction();
            try {
                redo_action = this.redo_stack.pop();
            } catch (IndexError e) { /* ignore IndexError! if empty: we already returned */ }
            this.undo_stack.push(redo_action);
            TextIter start;
            TextIter stop;
            if (redo_action is UndoableInsert) {
                this.get_iter_at_offset(out start, (redo_action as UndoableInsert).offset);
                this.insert(ref start, redo_action.text, -1); // entire text
                this.get_iter_at_offset(out stop,
                    (redo_action as UndoableInsert).offset + (redo_action as UndoableInsert).length
                );
                this.place_cursor(stop);
            } else {
                this.get_iter_at_offset(out start, (redo_action as UndoableDelete).start);
                this.get_iter_at_offset(out stop, (redo_action as UndoableDelete).end);
                this.delete(ref start, ref stop);
                this.place_cursor(start);
            }
            this.end_not_undoable_action();
            this.undo_in_progress = false;
        }

        /*
         * Clears the stacks for undos and redos.
         */
        public void clear_undo_redo_actions() {
            this.undo_stack.clear();
            this.redo_stack.clear();
        }
    }
}
