/* poppynotes@perichron.org +++ 2013-07-16
 * Helper class to register/load plugins (modules) and create plugin objects.
 */
using GLib;

namespace PoppyNotes {

    public class PluginRegistrar<T> : Object {

#if ( DEPLOY )
		const string PLUGIN_DIRECTORY = Config.PACKAGE_DATA_DIR + "/plugins";
#else
		const string PLUGIN_DIRECTORY = "data/plugins";
#endif
        
        public string path { get; private set; }

        private Type type;
        private Module module;

        private delegate Type RegisterPluginFunction (Module module);

        public PluginRegistrar(string plugin_name) {
            assert(Module.supported());
            this.path = Module.build_path(PLUGIN_DIRECTORY, plugin_name);
        }

        public bool load() {
            stdout.printf("Loading plugin with path: '%s'\n", this.path);

            this.module = Module.open(this.path, ModuleFlags.BIND_LAZY);
            if (this.module == null)
                return false;

            stdout.printf("Loaded module: '%s'\n", this.module.name());

            void* function;
            this.module.symbol("register_plugin", out function);
            RegisterPluginFunction register_plugin = (RegisterPluginFunction) function;

            this.type = register_plugin(this.module);
            stdout.printf("Plugin type: %s\n\n", this.type.name());
            return true;
        }

        public T new_object () {
            return Object.new(this.type);
        }
    }

}