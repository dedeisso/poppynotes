/* poppynotes@perichron.org +++ 2013-06-25
 * Interface for source views, i.e. views that show the content
 * in raw format (unformatted).
 * Example: content is of type HTML, document view interprets html
 * but source view just shows the unformatted content (html tags, ...).
 */
using Gtk;

namespace PoppyNotes {
    public interface ISourceView : Gtk.Frame, IView, IContentView {
    }
}