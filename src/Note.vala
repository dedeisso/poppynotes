/* poppynotes@perichron.org +++ 2013-06-25
 * Class representing a note.
 * Contains content and properties and a window (containing views),
 * to display the content and properties.
 * It also holds the filename where the note is stored, because it is
 * responsible for saving content/properties.
 */
using Gee;
using Gtk;

namespace PoppyNotes {
    public class Note {
        public string file { get; private set; }
		public int pos_x { get; private set; default = 100; }
        public int pos_y { get; private set; default = 100; }
		public int width { get; private set; default = 200; }
        public int height { get; private set; default = 200; }
		public string font_family { get; private set; default = "monospace"; }
        public float font_size { get; private set; default = 10.0f; }
        public string font_color { get; private set; default = "black"; }
        public string bg_color { get; private set; default = "white"; }
        public string content_type { get; private set; default = "plain"; }
		public string content { get; private set; default = ""; }

        protected IWindow window; // the window to show everything
        protected bool something_changed { get; set; default = false; } // flag: true if something (content, props) has changed since last save

        protected Note(string file) {
            this.file = file;
            this.attach_new_window();
        }
        public Note.with_properties(
                string file, int width, int height,
                int pos_x, int pos_y,
                string font_family, float font_size,
                string font_color, string bg_color,
                string content_type, string content) {
            this(file);
            if (width > 0 && width < 1000)
                this.width = width;
            else stderr.printf("The width of note '%s' should be >0 and <1000.", file);
            if (height > 0 && height < 1000)
                this.height = height;
            else stderr.printf("The height of note '%s' should be >0 and <1000.", file);
            if (pos_x > 0 && pos_x < 1000)
                this.pos_x = pos_x;
            else stderr.printf("The pos_x of note '%s' should be >0 and <1000.", file);
            if (pos_y > 0 && pos_y < 1000)
                this.pos_y = pos_y;
            else stderr.printf("The pos_y of note '%s' should be >0 and <1000.", file);
            this.font_family = font_family;
            if (font_size > 0.0f && font_size < 50.0f)
                this.font_size = font_size;
            else stderr.printf("The font_size of note '%s' should be >0.0 and <50.0.", file);
            this.font_color = font_color;
            this.bg_color = bg_color;
            this.content_type = content_type;
            this.content = content;

            this.attach_views_to_window();
        }

        /*
         * Show the note (window).
         */
		public void show() {
            // if I call show_all if the window is already showing,
            // the event coordinates in window.configure_event
            // are shifted by (1px, 28px)!! => only call show_all
            // if the window is invisible, otherwise present the window
            // to the user.
            if (!this.window.visible) {
                this.window.set_default_size(this.width, this.height);
                this.window.resize(this.width, this.height);
                this.window.move(this.pos_x, this.pos_y);
			    this.window.show_all();
            } else {
                // BUG in window.present! (https://bugzilla.gnome.org/show_bug.cgi?id=688830)
                // this.window.present();
                // workaround (from link above):
                uint32 st = Gdk.x11_get_server_time(this.window.get_window());
                this.window.present_with_time(st);
                //TODO test if the above really fixes it
            }
		}

        /*
         * Hide the note (window).
         */
		public void hide() {
            if (this.window.visible)
			    this.window.hide();
		}

        /*
         * Save the note (content and properties).
         */
		public void save() {
			var g = new Json.Generator();
			g.pretty = true;
			g.root = this.construct_json_node();
			try {
				g.to_file(this.file);
                this.something_changed = false;
			} catch (GLib.Error e) {
				stderr.printf("Error: %s\n", e.message);
			}
		}

        public signal void note_deleted(Note note);

/////////////////////////
//  Protected methods  //
/////////////////////////

        protected NoteProperties aggregate_props() {
            return NoteProperties() {
                width = this.width, height = this.height,
                pos_x = this.pos_x, pos_y = this.pos_y,
                font_family = this.font_family, font_size = this.font_size,
                font_color = this.font_color, bg_color = this.bg_color,
                content_type = this.content_type };
        }

        /*
         * Create a new window and connect to signals.
         */
        protected void attach_new_window() {
            this.window = WindowFactory.create_window();
            this.window.focus_out_event.connect((source, event) => {
                // window lost focus => save content and properties
                if (this.something_changed)
                    this.save();
                return true; // do not propagate any further
            });
            this.window.delete_note_request.connect(this.delete_note);
        }

        /*
         * Create new views and attach them to this.window.
         * Connect to the signals of the views.
         */
        protected void attach_views_to_window() {
            // create the views
            this.window.doc_view = ViewFactory.create_document_view(this.content_type);
            this.window.src_view = ViewFactory.create_source_view(this.content_type);
            this.window.prop_view = ViewFactory.create_properties_view();

            // set the content and properties to the views
            this.window.doc_view.content = content;
            this.window.src_view.content = content;
            this.window.prop_view.properties = this.aggregate_props();

            // connect to the views' signals
            this.window.doc_view.content_changed.connect((t, new_content) => {
                this.something_changed = true;
                this.content = new_content;
            });
            this.window.src_view.content_changed.connect((t, new_content) => {
                this.something_changed = true;
                this.content = new_content;
            });
            this.window.prop_view.properties_changed.connect((t, new_props) => {
                this.something_changed = true;
                this.width = new_props.width;
                this.height = new_props.height;
                this.pos_x = new_props.pos_x;
                this.pos_y = new_props.pos_y;
                this.font_family = new_props.font_family;
                this.font_size = new_props.font_size;
                this.font_color = new_props.font_color;
                this.bg_color = new_props.bg_color;
                if (this.content_type != new_props.content_type) {
                    this.content_type = new_props.content_type;
                    // if content_type has changed: destroy the current window
                    var ww = this.window;
                    // and create a new one
                    this.attach_new_window();
                    // with views able to handle the new content type
                    this.attach_views_to_window();
                    // and show the window again
                    this.show();
                    ww.hide(); // TODO should be destroyed, but SegmFault then
                }
            });

            Gtk.AccelGroup ag = new Gtk.AccelGroup();
            this.window.add_accel_group(ag);
            this.window.doc_view.add_accelerator("undo", ag, Gdk.Key.Z, Gdk.ModifierType.CONTROL_MASK, Gtk.AccelFlags.VISIBLE);
            this.window.doc_view.add_accelerator("redo", ag, Gdk.Key.Y, Gdk.ModifierType.CONTROL_MASK, Gtk.AccelFlags.VISIBLE);
            Gtk.AccelGroup ag2 = new Gtk.AccelGroup();
            this.window.add_accel_group(ag2);
            this.window.src_view.add_accelerator("undo", ag2, Gdk.Key.Z, Gdk.ModifierType.CONTROL_MASK, Gtk.AccelFlags.VISIBLE);
            this.window.src_view.add_accelerator("redo", ag2, Gdk.Key.Y, Gdk.ModifierType.CONTROL_MASK, Gtk.AccelFlags.VISIBLE);            
        }

        /*
         * Construct the Json.Node object from the properties and content.
         */
		protected Json.Node construct_json_node() {
			var p = new Json.Builder();
			p.begin_object();
				p.set_member_name("poppynotes_object_type");
				p.add_string_value("note");
				p.set_member_name("width");
				p.add_int_value((int64) this.width);
				p.set_member_name("height");
				p.add_int_value((int64) this.height);
				p.set_member_name("pos_x");
				p.add_int_value((int64) this.pos_x);
				p.set_member_name("pos_y");
				p.add_int_value((int64) this.pos_y);
				p.set_member_name("font_family");
				p.add_string_value(this.font_family);
				p.set_member_name("font_size");
				p.add_double_value((double) this.font_size);
				p.set_member_name("font_color");
				p.add_string_value(this.font_color);
				p.set_member_name("bg_color");
				p.add_string_value(this.bg_color);
				p.set_member_name("content_type");
				p.add_string_value(this.content_type);
				p.set_member_name("content");
				p.add_string_value(this.content);
			p.end_object();
			return p.get_root();
		}

        /*
         * Delete the note completely.
         * (Destroy the window, delete the file.)
         */
		protected void delete_note() {
            this.window.destroy();
            try {
                var f = File.new_for_path(this.file);
                f.delete();
            } catch (GLib.Error e) {
                stderr.printf("Could not delete note file %s!", this.file);
            }
            this.note_deleted(this);
        }
    }
}
