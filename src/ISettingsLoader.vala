/* poppynotes@perichron.org +++ 2013-06-25
 * Interface for classes that load settings (e.g. from special files).
 */
namespace PoppyNotes {
    public interface ISettingsLoader : GLib.Object {
        /*
         * Load the settings.
         */
        public abstract Settings load_settings();
    }
}