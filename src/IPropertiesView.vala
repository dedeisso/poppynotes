/* poppynotes@perichron.org +++ 2013-06-25
 * Interface for properties views, i.e. views that show the properties
 * of notes, e.g. content type, font size, window position, ...
 */
using Gtk;

namespace PoppyNotes {
    public interface IPropertiesView : Gtk.Frame, IView {
        /*
         * Get and set the properties this view should display.
         * //TODO how can i remove this owned. i actually want to get an unowned struct.
         */
        public abstract NoteProperties properties { owned get; set; }
        /*
         * Signal emitted when the properties have changed.
         */
        public signal void properties_changed(NoteProperties new_properties);
    }
}