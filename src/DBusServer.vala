/* poppynotes@perichron.org +++ 2013-07-07
 * DBus Server to let external programs control the indicator.
 */
namespace PoppyNotes {
    [DBus (name = "org.perichron.PoppyNotes")]
    public class DBusServer : Object {
        private App _app;
        public DBusServer(App app) {
            this._app = app;
        }
        public void show_all_notes() {
            this._app.show_all_notes();
        }
        public void hide_all_notes() {
            this._app.hide_all_notes();
        }
        public void new_note() {
            this._app.new_note();
        }
        public void show_settings() {
            this._app.show_settings();
        }

        private bool show_all = true; // false means hide_all
        public void show_hide_all_notes_toggle() {
            if (this.show_all)
                this.show_all_notes();
            else
                this.hide_all_notes();
            this.show_all = !this.show_all;
        }
    }
}
