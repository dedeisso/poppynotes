/* poppynotes@perichron.org +++ 2013-06-25
 * Class for loading notes, given some settings and creating new notes,
 * given some settings.
 */
using Gee;

namespace PoppyNotes {
    public class NoteLoader : GLib.Object, INoteLoader {
        private ArrayList<Note> _notes;

        public ArrayList<Note> load_notes(Settings settings) {
            this._notes = new ArrayList<Note>();
            // get the folder where the notes are stored
            var dir_name = settings.notes_directory;
            var dir = GLib.File.new_for_path(dir_name);
            if (dir.query_exists() &&
                dir.query_file_type(0) == FileType.DIRECTORY) {
                try {
                    var enumerator = dir.enumerate_children(FileAttribute.STANDARD_NAME, 0);
                    FileInfo file_info;
                    // for each file in the notes directory try to load it as a note
                    while ((file_info = enumerator.next_file ()) != null) {
                        try {
                            this._notes.add(this.load_note(dir_name + "/" + file_info.get_name()));
                        } catch (PNError e) {
                            //ignore error, the current file just seems to be no note-file
                            //stderr.printf (file_info.get_name() + ": " + e.message);
                        }
                    }
                } catch (GLib.Error e) {
                    stderr.printf("PoppyNotesError 10001: " + e.message);
                }
            }
            return this._notes;
        }

        public Note new_note(Settings settings) {
            var new_filename = this.generate_unique_note_filename(settings.notes_directory);
            var n = new Note.with_properties(settings.notes_directory + "/" + new_filename,
                                             settings.default_width,
                                             settings.default_height,
                                             settings.default_pos_x,
                                             settings.default_pos_y,
                                             settings.default_font_family,
                                             settings.default_font_size,
                                             settings.default_font_color,
                                             settings.default_bg_color,
                                             settings.default_content_type,
                                             "");
            n.save();
            return n;
        }

/////////////////////////
//  Protected methods  //
/////////////////////////

        /*
         * Try to load a note from the file given by its path.
         */
        protected Note load_note(string file) throws PNError {
            try {
                var p = new Json.Parser();
                p.load_from_file(file);
                Json.Node r = p.get_root();
                Json.Object note = r.get_object();
                var pnobjtype = note.get_string_member("poppynotes_object_type");
                if (pnobjtype == "note") {
			        int width = (int) note.get_int_member("width");
			        int height = (int) note.get_int_member("height");
			        int pos_x = (int) note.get_int_member("pos_x");
			        int pos_y = (int) note.get_int_member("pos_y");
                    string font_family = note.get_string_member("font_family");
                    float font_size = (float) note.get_double_member("font_size");
                    if (font_size == 0.0f) font_size = (float) note.get_int_member("font_size");
                    string font_color = note.get_string_member("font_color");
        			string bg_color = note.get_string_member("bg_color");
        			string ctype = note.get_string_member("content_type");
                    string content = note.get_string_member("content");
                    return new Note.with_properties(file, width, height, pos_x, pos_y,
                                        font_family, font_size, font_color, bg_color,
                                        ctype, content);
                } else
                    throw new PNError.WHILE_LOADING("This is no poppynotes note file.");
            } catch (GLib.Error e) {
                throw new PNError.WHILE_LOADING(e.message);
            }
        }

        /*
         * Generates a new unique note filename, that does not yet exist
         * in the given directory.
         * Returns the filename (without preceding directories).
         */
        protected string generate_unique_note_filename(string directory) {
			bool file_already_exists = true;
            string new_file = "";
			do {
				new_file = this.generate_random_note_filename();
				file_already_exists = GLib.File.new_for_path(directory + "/" + new_file).query_exists();
			} while (file_already_exists);
            return new_file;
        }

        /*
         * Generates a random note filename, that may exist or may not exist.
         */
		protected string generate_random_note_filename() {
			string chars = GLib.CharacterSet.DIGITS + GLib.CharacterSet.DIGITS +
				GLib.CharacterSet.a_2_z + GLib.CharacterSet.DIGITS;

			var builder = new GLib.StringBuilder();
			builder.append("pn");
			builder.append_c(chars[GLib.Random.int_range(0, 19)]);
			for (int i = 0; i < 5; i++)
				builder.append_c(chars[GLib.Random.int_range(0, (int32) chars.length - 1)]);
            builder.append(".note");
			return builder.str;
		}
    }
}