/* poppynotes@perichron.org +++ 2013-06-25
 * Main application class, responsible for all main actions:
 * * Show all notes
 * * Hide all notes
 * * Create new note
 * * Show settings
 * * Quit the application
 * Stores all notes and the settings.
 * Relies on an INoteLoader an ISettingsLoader
 */
using GLib;
using Gtk;
using Gee;

namespace PoppyNotes {
    public class App {

#if ( DEPLOY )
        const string ABOUT_UI_FILE = Config.PACKAGE_DATA_DIR + "/ui/about_dialog.ui";
#else
        const string ABOUT_UI_FILE = "ui/about_dialog.ui";
#endif

        protected INoteLoader note_loader;
        protected ISettingsLoader settings_loader;
        protected PoppyNotes.Settings settings; //the application settings
        protected ArrayList<Note> notes;        //all existing notes
        private bool _is_about_dialog_showing;   //flag if about_dialog is already being shown
        protected Gtk.AboutDialog about_dialog;

        public App() {
            this.note_loader = NoteLoaderProvider.get_note_loader();
            this.settings_loader = SettingsLoaderProvider.get_settings_loader();
            this.settings = this.settings_loader.load_settings();
			this.notes = this.note_loader.load_notes(this.settings);
			foreach(Note note in this.notes) {
				note.note_deleted.connect((note) => {
                    this.notes.remove(note);
                });
			}
            if (this.settings.show_on_startup)
                this.show_all_notes();
		}

        /*
         * Show all notes.
         */
        public void show_all_notes() {
			foreach(Note note in this.notes) {
				note.show();
			}
        }

        /*
         * Hide all notes.
         */
        public void hide_all_notes() {
			foreach(Note note in this.notes) {
				note.hide();
			}
        }

        /*
         * Show a new note.
         */
        public void new_note() {
            var n = this.note_loader.new_note(this.settings);
            this.notes.add(n);
            n.show();
        }

        /*
         * Show the settings dialog.
         */
        public void show_settings() {
            this.settings.show_edit_dialog();
        }

        /*
         * Show the about dialog.
         */
        public void show_about() {
            if (!this._is_about_dialog_showing) {
	            try {
		            var builder = new Builder();
		            builder.add_from_file(ABOUT_UI_FILE);
		            var win = builder.get_object("about_dialog") as Gtk.AboutDialog;
                    win.delete_event.connect(() => {
			            this._is_about_dialog_showing = false; return false;
                    });
                    win.response.connect((source, response_id) => {
                        if (response_id == -6) { // button close
                            this._is_about_dialog_showing = false;
                            win.destroy();
                        }
                    });
                    win.version = Config.PACKAGE_VERSION;
                    
                    this.about_dialog = win;
                    win.show_all();
                    this._is_about_dialog_showing = true;
	            } catch (GLib.Error e) {
		            stderr.printf("Could not load UI: %s\n", e.message);
	            }
            } else {
                this.about_dialog.present();
            }
        }

        /*
         * Quit the application.
         */
        public void quit() {
            //TODO check/test if there is anything to do
            // the notes should be saved automatically
        }

/////////////////////////
//  Protected methods  //
/////////////////////////

    }
}