/* poppynotes@perichron.org +++ 2013-06-25
 * Factory class to construct window instances to display notes.
 * Uses Gtk.Builder to load the UI structure from a Glade UI file.
 */
using Gtk;

namespace PoppyNotes {
    public class WindowFactory {

#if ( DEPLOY )
		const string WINDOW_UI_FILE = Config.PACKAGE_DATA_DIR + "/ui/note_window.ui";
#else
		const string WINDOW_UI_FILE = "ui/note_window.ui";
#endif

        /*
         * Create an instance of a note window by using Gtk.Builder.
         */
        public static IWindow create_window() {
			try {
				var builder = new Builder();
				builder.add_from_file(WINDOW_UI_FILE);
				var win = builder.get_object("window") as IWindow;
				builder.connect_signals(win);
                return win;
			} catch (Error e) {
				stderr.printf ("Could not load UI: %s\n", e.message);
                return new PoppyNotes.Window();
			}
        }
    }
}