/* poppynotes@perichron.org +++ 2013-06-25
 * Interface for document views, i.e. views that show the content
 * not in raw format but somehow formatted.
 * Example: content is of type HTML, document view interprets html
 * and shows the formatted content.
 * A document view is still allowed to edit the content. E.g. a user may
 * use the document view's WYSIWYG editor to edit the content.
 */
using Gtk;

namespace PoppyNotes {
    public interface IDocumentView : Gtk.Frame, IView, IContentView {
    }
}