/* poppynotes@perichron.org +++ 2013-06-25
 * Class containing static utility methods which may be used by different
 * parts of the application.
 */
using Gtk;

namespace PoppyNotes {
    public class Util : GLib.Object {
        
        /*
         * Get a widget by its name out of the (given) UI structure
         */
        public static Gtk.Widget? get_child_by_name(Gtk.Widget parent, string name) {
            //parent.name just gets me the Typename 'ToggleToolButton' !!!
            //see: http://stackoverflow.com/questions/3489520/python-gtk-widget-name
            if (parent.get_name() == name) return parent;
            if (parent is Gtk.Container) {
                foreach (var c in (parent as Gtk.Container).get_children()) {
                    var result = get_child_by_name(c, name);
                    if (result != null) return result;
                    else continue;
                }
            }
            return null;
        }
        /*
         * Get the font family out of a font string (of the format "$font_family $font_size").
         */
        public static string font_to_family(string font) {
            return font.slice(0, font.last_index_of_char(' '));
        }
        /*
         * Get the font size out of a font string (of the format "$font_family $font_size").
         */
        public static float font_to_size(string font) {
            return (float)double.parse(font.substring(font.last_index_of_char(' ') + 1));
        }
    }
}