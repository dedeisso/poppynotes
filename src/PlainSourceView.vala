/* poppynotes@perichron.org +++ 2013-06-25
 * Source view for content type 'plain'.
 * It just takes the content and displays it without any further formatting.
 */
using Gtk;

namespace PoppyNotes {
    public class PlainSourceView : Gtk.Frame, IView, IContentView, ISourceView {

        private bool _content_set_directly = false; // flag to distinguish between change
                                                    // by text input and change by prop set
        private string _content;
        public string content {
            get { return this._content; }
            set {
                this._content = value;
                this._content_set_directly = true;
                if (this.textview is UndoableTextView) {
                    //(this.textview.buffer as UndoableTextBuffer).begin_not_undoable_action();
                    this.textview.buffer.text = value;
                    (this.textview.buffer as UndoableTextBuffer).clear_undo_redo_actions();
                    //(this.textview.buffer as UndoableTextBuffer).end_not_undoable_action();
                } else
                    this.textview.buffer.text = value;
            }
        }

        public string default_font_family {
            set {
                var fd = Pango.FontDescription.from_string(value);
                this.textview.override_font(fd);
            }
        }
        public float default_font_size {
            set {
                var fd = new Pango.FontDescription();
                fd.set_absolute_size(value * Pango.SCALE);
                this.textview.override_font(fd);
            }
        }
        public string default_font_color {
            set {
                var col = Gdk.RGBA(); col.parse(value);
                this.textview.override_color(Gtk.StateFlags.NORMAL, col);
                // in order to see any selected text!! (+bg_color)
                col.parse("white"); this.textview.override_color(Gtk.StateFlags.SELECTED, col);
            }
        }
        public string default_bg_color {
            set {
                var col = Gdk.RGBA(); col.parse(value);
                this.textview.override_background_color(Gtk.StateFlags.NORMAL, col);
                // in order to see any selected text!! (+font_color)
                col.parse("orange"); this.textview.override_background_color(Gtk.StateFlags.SELECTED, col);
            }
        }

        // the internal TextView object
        private Gtk.TextView _textview;
        protected Gtk.TextView textview {
            get {
                if (this._textview == null)
                    this._textview = Util.get_child_by_name(this, "textview_src") as Gtk.TextView;
                return this._textview;
            }
        }

        public void init() {
            //this won't work in a construct{} block since
            //at construction time there will be no child elements yet

            //get textview out of UI-structure and connect signal
            this.textview.buffer.changed.connect((source) => {
                if (!this._content_set_directly) {
                    Gtk.TextIter s_iter, e_iter;
                    this.textview.buffer.get_start_iter(out s_iter);
                    this.textview.buffer.get_end_iter(out e_iter);
                    var new_content = this.textview.buffer.get_text(s_iter, e_iter, true);
                    if (this._content != new_content) {
                        this._content = new_content;
                        this.content_changed(this.content); // emit signal
                    }
                }
                this._content_set_directly = false;
            });

            // connect to own signals (maybe activated by user)
            this.undo.connect(() => {
                if (this.textview is UndoableTextView)
                    (this.textview as UndoableTextView).undo();
            });
            this.redo.connect(() => {
                if (this.textview is UndoableTextView)
                    (this.textview as UndoableTextView).redo();
            });
        }
    }
}