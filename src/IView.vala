/* poppynotes@perichron.org +++ 2013-06-25
 * Interface for views, i.e. Gtk.Frames that can display content
 * or properties of a note.
 */
using Gtk;

namespace PoppyNotes {
    public interface IView : Gtk.Frame {
        /*
         * Initialisation method.
         * To be called after the view has been built by the Gtk.Builder,
         * i.e. after all child widgets have been added to the view.
         * (should be called by a ViewFactory)
         */
        public abstract void init();
    }
}