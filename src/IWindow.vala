/* poppynotes@perichron.org +++ 2013-06-25
 * Interface for window classes that contain certain views to display
 * a note's content and properties.
 */
using Gtk;

namespace PoppyNotes {
    public interface IWindow : Gtk.Window {
        /*
         * Get and set the document view.
         */
        public abstract IDocumentView doc_view { get; set; }
        /*
         * Get and set the source view.
         */
        public abstract ISourceView src_view { get; set; }
        /*
         * Get and set the properties view.
         */
        public abstract IPropertiesView prop_view { get; set; }
        /*
         * Signal that gets emitted when the user wants to delete the note.
         */
        public signal void delete_note_request();
    }
}