/* poppynotes@perichron.org +++ 2013-06-25
 * Properties view to display a note's properties and let the user edit them.
 */
using Gtk;

namespace PoppyNotes {
    public class PropertiesView : Gtk.Frame, IView, IPropertiesView {

        public NoteProperties properties {
            owned get { return NoteProperties() {
                width = int.parse((this.widget_width as Gtk.Label).label),
                height = int.parse((this.widget_height as Gtk.Label).label),
                pos_x = int.parse((this.widget_pos_x as Gtk.Label).label),
                pos_y = int.parse((this.widget_pos_y as Gtk.Label).label),
                font_family = Util.font_to_family((this.widget_font as Gtk.FontButton).font_name),
                font_size = Util.font_to_size((this.widget_font as Gtk.FontButton).font_name),
                font_color = (this.widget_font_color as Gtk.ColorButton).rgba.to_string(),
                bg_color = (this.widget_bg_color as Gtk.ColorButton).rgba.to_string(),
                content_type = (this.widget_content_type as Gtk.Entry).text };
            }
            set {
                (this.widget_width as Gtk.Label).label = value.width.to_string();
                (this.widget_height as Gtk.Label).label = value.height.to_string();
                (this.widget_pos_x as Gtk.Label).label = value.pos_x.to_string();
                (this.widget_pos_y as Gtk.Label).label = value.pos_y.to_string();
                (this.widget_font as Gtk.FontButton).font_name = value.font_family + " " + value.font_size.to_string();
                var col = Gdk.RGBA();
                col.parse(value.font_color);
                (this.widget_font_color as Gtk.ColorButton).rgba = col;
                col.parse(value.bg_color);
                (this.widget_bg_color as Gtk.ColorButton).rgba = col;
                (this.widget_content_type as Gtk.Entry).text = value.content_type;
                this.properties_changed(this.properties);
            }
        }
           
        
        public void init() {
            //nothing to do here
        }

        private Gtk.Widget _widget_width;
        protected Gtk.Widget widget_width {
            get {
                if (this._widget_width == null)
                    this._widget_width = Util.get_child_by_name(this, "label_width") as Gtk.Widget;
                return this._widget_width;
            }
        }
        private Gtk.Widget _widget_height;
        protected Gtk.Widget widget_height {
            get {
                if (this._widget_height == null)
                    this._widget_height = Util.get_child_by_name(this, "label_height") as Gtk.Widget;
                return this._widget_height;
            }
        }
        private Gtk.Widget _widget_pos_x;
        protected Gtk.Widget widget_pos_x {
            get {
                if (this._widget_pos_x == null)
                    this._widget_pos_x = Util.get_child_by_name(this, "label_pos_x") as Gtk.Widget;
                return this._widget_pos_x;
            }
        }
        private Gtk.Widget _widget_pos_y;
        protected Gtk.Widget widget_pos_y {
            get {
                if (this._widget_pos_y == null)
                    this._widget_pos_y = Util.get_child_by_name(this, "label_pos_y") as Gtk.Widget;
                return this._widget_pos_y;
            }
        }
        private Gtk.Widget _widget_font;
        protected Gtk.Widget widget_font {
            get {
                if (this._widget_font == null)
                    this._widget_font = Util.get_child_by_name(this, "fontbutton_font") as Gtk.Widget;
                return this._widget_font;
            }
        }
        private Gtk.Widget _widget_font_color;
        protected Gtk.Widget widget_font_color {
            get {
                if (this._widget_font_color == null)
                    this._widget_font_color = Util.get_child_by_name(this, "colorbutton_font_color") as Gtk.Widget;
                return this._widget_font_color;
            }
        }
        private Gtk.Widget _widget_bg_color;
        protected Gtk.Widget widget_bg_color {
            get {
                if (this._widget_bg_color == null)
                    this._widget_bg_color = Util.get_child_by_name(this, "colorbutton_bg_color") as Gtk.Widget;
                return this._widget_bg_color;
            }
        }
        private Gtk.Widget _widget_content_type;
        protected Gtk.Widget widget_content_type {
            get {
                if (this._widget_content_type == null)
                    this._widget_content_type = Util.get_child_by_name(this, "entry_content_type") as Gtk.Widget;
                return this._widget_content_type;
            }
        }

//////////////////////////
//  UI handler methods  //
//////////////////////////
        
        [CCode(instance_pos=-1)]
        public void on_button_apply_clicked(Gtk.Button source) {
            // just emit signal, window should adjust position, size, ...
            // note should store/save the new properties
            this.properties_changed(this.properties);
        }
    }
}