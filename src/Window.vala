/* poppynotes@perichron.org +++ 2013-06-25
 * Class for displaying a notes contents and properties in a notes window.
 */
using Gtk;

namespace PoppyNotes {
    public class Window : Gtk.Window, IWindow {
        
        //at every point in time should hold the two views that are not shown right now
        private Gtk.Box _tmp_view_parent = new Gtk.Box(Gtk.Orientation.VERTICAL, 3);

        // the document view
        private IDocumentView _doc_view;
        public IDocumentView doc_view {
            get { return this._doc_view; }
            set { //only allowed once
                if (this._doc_view == null) {
                    this._doc_view = value;
                    this._tmp_view_parent.child = this._doc_view;
                    if (this.button_docview.active)
                        this._doc_view.reparent(this.frame_main_alignment);
                    this._doc_view.content_changed.connect((t, new_content) => {
                        // synchronise content between src and doc view
                        if (this.src_view.content != this.doc_view.content)
                            this.src_view.content = new_content;
                    });
                }
            }
        }

        // the source view
        private ISourceView _src_view;
        public ISourceView src_view {
            get { return this._src_view; }
            set { //only allowed once
                if (this._src_view == null) {
                    this._src_view = value;
                    this._tmp_view_parent.child = this._src_view;
                    if (this.button_srcview.active)
                        this._src_view.reparent(this.frame_main_alignment);
                    this._src_view.content_changed.connect((t, new_content) => {
                        // synchronise content between src and doc view
                        if (this.doc_view.content != this.src_view.content)
                            this.doc_view.content = new_content;
                    });
                }
            }
        }

        // the properties view
        private IPropertiesView _prop_view;
        public IPropertiesView prop_view {
            get { return this._prop_view; }
            set { //only allowed once
                if (this._prop_view == null) {
                    this._prop_view = value;
                    this._tmp_view_parent.child = this._prop_view;
                    if (this.button_propview.active)
                        this._prop_view.reparent(this.frame_main_alignment);
                    this._prop_view.properties_changed.connect((t, new_props) => {
                        // Do not adjust size and position!!
                        // these should not be edited from the props view!!
                        // otherwise: cyclic signal!!
                        // Change other stuff like color
                        this.doc_view.default_font_family = new_props.font_family;
                        this.doc_view.default_font_size = new_props.font_size;
                        this.doc_view.default_font_color = new_props.font_color;
                        this.doc_view.default_bg_color = new_props.bg_color;
                        this.src_view.default_font_family = new_props.font_family;
                        this.src_view.default_font_size = new_props.font_size;
                        this.src_view.default_font_color = new_props.font_color;
                        this.src_view.default_bg_color = new_props.bg_color;
                    });
                }
            }
        }

        private Gtk.ToggleToolButton _button_docview;
        public Gtk.ToggleToolButton button_docview {
            get {
                if (this._button_docview == null)
                    this._button_docview = Util.get_child_by_name(this, "toolbutton_docview") as Gtk.ToggleToolButton;
                return this._button_docview;
            }
        }
        private Gtk.ToggleToolButton _button_srcview;
        public Gtk.ToggleToolButton button_srcview {
            get {
                if (this._button_srcview == null)
                    this._button_srcview = Util.get_child_by_name(this, "toolbutton_srcview") as Gtk.ToggleToolButton;
                return this._button_srcview;
            }
        }
        private Gtk.ToggleToolButton _button_propview;
        public Gtk.ToggleToolButton button_propview {
            get {
                if (this._button_propview == null)
                    this._button_propview = Util.get_child_by_name(this, "toolbutton_propview") as Gtk.ToggleToolButton;
                return this._button_srcview;
            }
        }
        private Gtk.ToolButton _button_delete;
        public Gtk.ToolButton button_delete {
            get {
                if (this._button_delete == null)
                    this._button_delete = Util.get_child_by_name(this, "toolbutton_delete") as Gtk.ToolButton;
                return this._button_delete;
            }
        }
        private Gtk.Frame _frame_main;
        public Gtk.Frame frame_main {
            get {
                if (this._frame_main == null)
                    this._frame_main = Util.get_child_by_name(this, "frame_main") as Gtk.Frame;
                return this._frame_main;
            }
        }
        private Gtk.Alignment _frame_main_alignment;
        public Gtk.Alignment frame_main_alignment {
            get {
                if (this._frame_main_alignment == null)
                    this._frame_main_alignment = Util.get_child_by_name(this, "frame_main_alignment") as Gtk.Alignment;
                return this._frame_main_alignment;
            }
        }

        construct {
            //I can not access any child widgets yet! the Gtk.Builder will
            //add them after object creation!
            //(see IView for how I solved this there)

            this.delete_event.connect(this.hide_on_delete);
            //the upper is the same as the following:
            /*this.delete_event.connect(() => {
				this.hide(); return true;
            });*/
            this.configure_event.connect((source, event) => {
                if (this.visible) { // do nothing if window is invisible (=> no resize/move)
                    var props = this.prop_view.properties;
                    int x, y;
                    this.get_position(out x, out y);
                    if (props.pos_x != x || props.pos_y != y ||
                        props.width != event.width || props.height != event.height) { // do nothing if nothing has changed
                        props.pos_x = x;
                        props.pos_y = y;
                        props.width = event.width;
                        props.height = event.height;
                        this.prop_view.properties = props;
                    }
                }
                return false;
            });
        }
        
        public Window() {
            //when creating with Gtk.Builder the constructor is never called!!
            //therefor use the construct{} block ("GObject-Style Construction")
            Object();
        }

//////////////////////////
//  UI handler methods  //
//////////////////////////

        [CCode(instance_pos=-1)]
        public void on_toolbutton_docview_toggled(Gtk.ToggleToolButton source) {
            if (this.doc_view != null) {
                var prev_frame = this.frame_main_alignment.get_child();
                prev_frame.reparent(this._tmp_view_parent);
                this.doc_view.reparent(this.frame_main_alignment);
                if (prev_frame == this.prop_view) {
                    //TODO resize window (propview resizes it if propview's too large)
    /*                int w, h;
                    this.get_default_size(out w, out h);
                    this.resize(w, h);*/
                }
            }
        }

        [CCode(instance_pos=-1)]
        public void on_toolbutton_srcview_toggled(Gtk.ToggleToolButton source) {
            if (this.src_view != null) {
                this.frame_main_alignment.get_child().reparent(this._tmp_view_parent);
                this.src_view.reparent(this.frame_main_alignment);
                //TODO resize window (propview resizes it if propview's too large)
/*                int w, h;
                this.get_default_size(out w, out h);
                this.resize(w, h);*/
            }
        }

        [CCode(instance_pos=-1)]
        public void on_toolbutton_propview_toggled(Gtk.ToggleToolButton source) {
            if (this.prop_view != null) {
                this.frame_main_alignment.get_child().reparent(this._tmp_view_parent);
                this.prop_view.reparent(this.frame_main_alignment);
            }
        }

        [CCode(instance_pos=-1)]
        public void on_toolbutton_delete_clicked(Gtk.ToolButton source) {
            var d = new Gtk.MessageDialog(this, Gtk.DialogFlags.MODAL, Gtk.MessageType.QUESTION, Gtk.ButtonsType.YES_NO,
                                          _("Do you really want to delete this note?"));
            d.title = _("Delete");
            d.response.connect((response_id) => {
                switch (response_id) {
				    case Gtk.ResponseType.YES:
					    this.delete_note_request();
					    break;
				    case Gtk.ResponseType.NO:
					    // do nothing
					    break;
			    }
                d.destroy();
            });
            d.show();
        }
    }
}
