/* poppynotes@perichron.org +++ 2013-06-25
 * Application Indicator; main entry point for the application.
 * Shows an app indicator menu and handles the clicks to the menu entries.
 * Holds an instance of a PoppyNotes.App class.
 */
using GLib;
using Gtk;
using AppIndicator;

namespace PoppyNotes {
    public class Indicator {
#if ( DEPLOY )
		const string MENU_UI_FILE = Config.PACKAGE_DATA_DIR + "/ui/indicator_menu.ui";
#else
		const string MENU_UI_FILE = "ui/indicator_menu.ui";
#endif

		protected AppIndicator.Indicator indicator;
        protected App app;       // the application object
        protected Gtk.Menu menu; // the menu
        
		public Indicator() {

			this.app = new App();

			//TODO: (maybe) create indicator attention icon (not needed now)
			// my stuff gets deployed to /usr/local/share/icons/...
			// usually the icons are in /usr/share/icons/... => Problem?
			// if so, do configure --prefix=/usr (i think this should do it)
            this.indicator = new AppIndicator.Indicator("PoppyNotes", "poppynotes",
                                             IndicatorCategory.OTHER);
            this.indicator.set_status(IndicatorStatus.ACTIVE);
            this.indicator.set_attention_icon("poppynotes");

            try {
                this.load_menu();
                this.indicator.set_menu(this.menu);
			} catch (Error e) {
				stderr.printf ("Could not load UI: %s\n", e.message);
			} 
		}

        public static int main(string[] args) {
            //dirname of executable:?
            //string settings_dir = Path.get_dirname (args[0]);

			Intl.setlocale(LocaleCategory.MESSAGES, "");
			Intl.textdomain(Config.GETTEXT_PACKAGE);
			Intl.bind_textdomain_codeset(Config.GETTEXT_PACKAGE, "UTF-8"); // encoding
			//where to find the message catalogs
#if ( DEPLOY )
			Intl.bindtextdomain(Config.GETTEXT_PACKAGE, Config.PACKAGE_LOCALE_DIR);
#else
			Intl.bindtextdomain(Config.GETTEXT_PACKAGE, "po");
#endif
			
            Gtk.init(ref args);
			var ind = new PoppyNotes.Indicator(); // yes, I need this instance!!

            // start DBus-Server
            Bus.own_name(BusType.SESSION, "org.perichron.PoppyNotes", BusNameOwnerFlags.NONE,
                (conn) => { // bus acquired
                    try {
                        conn.register_object ("/org/perichron/poppynotes", new DBusServer(ind.app));
                    } catch (IOError e) {
                        stderr.printf ("DBus Error! Could not register service.\n");
                    }
                },
                () => { /* name acquired */ },
                () => stderr.printf("DBus Error! Could not aquire name.\n"));

            Gtk.main(); // start MainLoop
            
            return 0;
        }

/////////////////////////
//  Protected methods  //
/////////////////////////

        /*
         * Load the menu from a Glade UI file.
         */
		protected void load_menu() throws Error {
			var builder = new Builder();
			builder.add_from_file(MENU_UI_FILE);
			builder.connect_signals(this); // (signal handlers at the bottom)

			this.menu = builder.get_object("indicator_menu") as Gtk.Menu;
		}

//////////////////////////
//  UI signal handlers  //
//////////////////////////

		[CCode (instance_pos = -1)]
		public void on_destroy(Widget window) {
            stderr.printf("This IS actually reached!!\n");
			Gtk.main_quit();
		}
		[CCode(instance_pos=-1)]
		public void on_menuitem_showallnotes_activate(Gtk.MenuItem source) {
			//this.indicator.set_status(IndicatorStatus.ATTENTION);
			this.app.show_all_notes();
		}
		[CCode(instance_pos=-1)]
		public void on_menuitem_hideallnotes_activate(Gtk.MenuItem source) {
			this.app.hide_all_notes();
		}
		[CCode(instance_pos=-1)]
		public void on_menuitem_newnote_activate(Gtk.MenuItem source) {
			this.app.new_note();
		}
		[CCode(instance_pos=-1)]
		public void on_menuitem_settings_activate(Gtk.MenuItem source) {
			this.app.show_settings();
		}
		[CCode(instance_pos=-1)]
		public void on_menuitem_about_activate(Gtk.MenuItem source) {
			this.app.show_about();
		}
		[CCode(instance_pos=-1)]
		public void on_menuitem_exit_activate(Gtk.MenuItem source) {
			this.indicator.set_status(IndicatorStatus.PASSIVE);
			this.app.quit();
            Gtk.main_quit();
		}
    }
}