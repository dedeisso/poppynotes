/* poppynotes@perichron.org +++ 2013-06-25
 * TextView that makes use of the UndoableTextBuffer and
 * therefor provides undo and redo functionallity.
 * There are two action signals 'undo' and 'redo' to which
 * accelerators can be connected. When these signals are activated
 * the TextView performs an undo and redo action, respectively.
 */
using Gtk;

namespace PoppyNotes {

    public class UndoableTextView : Gtk.TextView {

        [Signal (action=true)]
        public signal void undo();

        [Signal (action=true)]
        public signal void redo();

        public UndoableTextView() {
            this.with_buffer(new UndoableTextBuffer());
        }
        public UndoableTextView.with_buffer(UndoableTextBuffer buffer) {
            this.buffer = buffer;
            this.undo.connect(buffer.undo);
            this.redo.connect(buffer.redo);
        }

        construct {
            var _buffer = new UndoableTextBuffer();
            this.undo.connect(_buffer.undo);
            this.redo.connect(_buffer.redo);
            this.buffer = _buffer;
        }
    }
}
