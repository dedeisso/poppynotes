/* poppynotes@perichron.org +++ 2013-06-25
 * Factory class to construct view instances to display notes' content/properties.
 * Uses Gtk.Builder to load the UI structure from Glade UI files.
 */
using Gtk;

namespace PoppyNotes {
    public class ViewFactory {

#if ( DEPLOY )
		const string DOCVIEW_PLAIN_UI_FILE = Config.PACKAGE_DATA_DIR + "/ui/document_view_plain.ui";
        const string SRCVIEW_PLAIN_UI_FILE = Config.PACKAGE_DATA_DIR + "/ui/source_view_plain.ui";
        const string PROPVIEW_UI_FILE = Config.PACKAGE_DATA_DIR + "/ui/properties_view.ui";
#else
		const string DOCVIEW_PLAIN_UI_FILE = "ui/document_view_plain.ui";
        const string SRCVIEW_PLAIN_UI_FILE = "ui/source_view_plain.ui";
        const string PROPVIEW_UI_FILE = "ui/properties_view.ui";
#endif

        /*
         * Create an instance of a document view by using Gtk.Builder.
         */
        public static IDocumentView create_document_view(string content_type) {
            switch (content_type) {
            case "plain":
            case "text":
            case "text/plain":
            case "plain_text":
                return _create_plain_document_view();
            default:
                stderr.printf("There is no document view available to handle contents of type %s!\n", content_type);
                stderr.printf("   Trying to find a plugin providing a handler!\n");
                var registrar = new PluginRegistrar<IDocumentView>("document_view_" + content_type);
                if (!registrar.load()) {
                    stderr.printf("   No plugin (%s) found to provide handler for contents of type %s!\n", "document_view_" + content_type, content_type);
                    stderr.printf("   => Using plain document view.");
                    return _create_plain_document_view();
                } else {
                    return registrar.new_object() as IDocumentView;
                }
            }
        }
        private static IDocumentView _create_plain_document_view() {
		    try {
			    var builder = new Builder();
			    builder.add_from_file(DOCVIEW_PLAIN_UI_FILE);
			    var win = builder.get_object("frame_document_plain") as IDocumentView;
			    builder.connect_signals(win);
                win.init();
                return win;
		    } catch (Error e) {
			    stderr.printf ("Could not load UI: %s\n", e.message);
                return new PlainDocumentView();
		    }
        }

        /*
         * Create an instance of a source view by using Gtk.Builder.
         */
        public static ISourceView create_source_view(string content_type) {
            switch (content_type) {
            case "plain":
            case "plain_text":
            case "text":
                return _create_plain_source_view();
            default:
                stderr.printf("There is no source view available to handle contents of type %s!\n", content_type);
                stderr.printf("   Trying to find a plugin providing a handler!\n");
                var registrar = new PluginRegistrar<ISourceView>("source_view_" + content_type);
                if (!registrar.load()) {
                    stderr.printf("   No plugin (%s) found to provide handler for contents of type %s!\n", "source_view_" + content_type, content_type);
                    stderr.printf("   => Using plain source view.");
                    return _create_plain_source_view();
                } else {
                    return registrar.new_object() as ISourceView;
                }
            }
        }
        private static ISourceView _create_plain_source_view() {
		    try {
			    var builder = new Builder();
			    builder.add_from_file(SRCVIEW_PLAIN_UI_FILE);
			    var win = builder.get_object("frame_source_plain") as ISourceView;
			    builder.connect_signals(win);
                win.init();
                return win;
		    } catch (Error e) {
			    stderr.printf ("Could not load UI: %s\n", e.message);
                return new PlainSourceView();
		    }
        }

        /*
         * Create an instance of a properties view by using Gtk.Builder.
         */
        public static IPropertiesView create_properties_view() {
		    try {
			    var builder = new Builder();
			    builder.add_from_file(PROPVIEW_UI_FILE);
			    var win = builder.get_object("frame_properties") as IPropertiesView;
			    builder.connect_signals(win);
                win.init();
                return win;
		    } catch (Error e) {
			    stderr.printf ("Could not load UI: %s\n", e.message);
                return new PropertiesView();
		    }
        }
    }
}
