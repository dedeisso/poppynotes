/* poppynotes@perichron.org +++ 2013-06-25
 * Interface for classes that load notes (e.g. from specially structured files)
 * and create new notes, given some settings.
 */
using Gee;

namespace PoppyNotes {
    public interface INoteLoader : GLib.Object {
        /*
         * Load all notes given the settings.
         */
        public abstract ArrayList<Note> load_notes(Settings settings);
        /*
         * Create a new note given the settings.
         */
        public abstract Note new_note(Settings settings);
    }
}