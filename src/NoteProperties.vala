/* poppynotes@perichron.org +++ 2013-06-25
 * Struct to aggregate a note's properties.
 */
namespace PoppyNotes {
    public struct NoteProperties {
        public int width;
        public int height;
        public int pos_x;
        public int pos_y;
        public string font_family;
        public float font_size;
        public string font_color;
        public string bg_color;
        public string content_type;
    }
}