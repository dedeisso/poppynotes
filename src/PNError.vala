/* poppynotes@perichron.org +++ 2013-06-25
 * Errordomain to use for poppy notes errors.
 */
using Gtk;

namespace PoppyNotes {
	public errordomain PNError {
		WHILE_LOADING,
		WHILE_SAVING
	}
}
