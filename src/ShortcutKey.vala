/* poppynotes@perichron.org +++ 2013-07-08
 * Struct representing shortcut keys with methods to
 * load from a string representation and to give a string representation.
 */
namespace PoppyNotes {

    public struct ShortcutKey {
        public bool enabled;
        public bool ctrl;
        public bool alt;
        public bool shift;
        public bool super;
        public char charnum;
        public string to_string() {
            if (enabled)
                return (ctrl ? "<Primary>" : "") +
                        (alt ? "<Alt>" : "") +
                        (shift ? "<Shift>" : "") +
                        (super ? "<Super>" : "") +
                        charnum.to_string();
            else
                return "";
        }
        public void load_from_string(string key) {
            if (key == "") { enabled = false; return; }
            enabled = true;
            if (key.down().contains("<primary>")) ctrl = true;
            if (key.down().contains("<alt>"))     alt = true;
            if (key.down().contains("<shift>"))   shift = true;
            if (key.down().contains("<super>"))   super = true;
            charnum = key.down().replace("<primary>", "").replace("<alt>", "").replace("<shift>", "").replace("<super>", "").get(0);
        }
        public static ShortcutKey from_string(string key) {
            var s = ShortcutKey();
            s.load_from_string(key);
            return s;
        }
    }
}