/* poppynotes@perichron.org +++ 2013-06-25
 * Interface for content views, which is document views and source views.
 */
using Gtk;

namespace PoppyNotes {
    public interface IContentView : Gtk.Frame, IView {
        /*
         * Get and set the content.
         */
        public abstract string content { get; set; }

        /*
         * Set the note's font_family property.
         * The views do not have to change anything if they do not want.
         * E.g. the document view might interpret the content, use the
         * font attributes given there and ignore these default values.
         * The source view on the other side might want to actually
         * use these values.
         */
        public abstract string default_font_family { set; }
        /*
         * Set the note's font_size property. (See default_font_family.)
         */
        public abstract float default_font_size { set; }
        /*
         * Set the note's font_color property. (See default_font_family.)
         */
        public abstract string default_font_color { set; }
        /*
         * Set the note's bg_color property. (See default_font_family.)
         */
        public abstract string default_bg_color { set; }
        
        /*
         * Signal emitted when content has changed.
         */
        public signal void content_changed(string new_content);

        /*
         * Action signal to activate by user (e.g. by accelerator).
         * The content views do not have to provide undo functionallity though!
         */
        [Signal (action=true)]
        public signal void undo();

        /*
         * Action signal to activate by user (e.g. by accelerator).
         * The content views do not have to provide redo functionallity though!
         */
        [Signal (action=true)]
        public signal void redo();
    }
}