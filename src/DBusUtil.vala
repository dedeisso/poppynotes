/* poppynotes@perichron.org +++ 2013-07-07
 * DBus client to control the indicator externally.
 * Used to show and hide all notes.
 * (To set an accelerator (keyboard shortcut) use the system's
 *  System Settings -> Keyboard -> Shortcuts)
 */
namespace PoppyNotes {

    public class DBusUtil {

        public const string DBUS_ACTION_SHOWHIDE = "-showhide";
        public const string DBUS_ACTION_SHOW     = "-showall";
        public const string DBUS_ACTION_HIDE     = "-hideall";
        public const string DBUS_ACTION_NEWNOTE  = "-newnote";
        public const string DBUS_ACTION_SETTINGS = "-settings";

    }
}
