/* poppynotes@perichron.org +++ 2013-06-25
 * Class holding the application settings and being able to show and let
 * the user edit these settings.
 * Holds the filename where the settings are stored, since it is
 * responsible for saving the settings.
 */
using Gtk;
using GConf;

namespace PoppyNotes {
    public class Settings {

#if ( DEPLOY )
        const string SETTINGS_UI_FILE = Config.PACKAGE_DATA_DIR + "/ui/settings_dialog.ui";
        const string PN_DBUS_APPLICATION = "poppynotes_dbusclient";
        //TODO change PACKAGE_DATA_DIR/../applications to something else!!
        const string PN_DESKTOP_FILE = Config.PACKAGE_DATA_DIR + "/../applications/poppynotes.desktop";
#else
        const string SETTINGS_UI_FILE = "ui/settings_dialog.ui";
        const string PN_DBUS_APPLICATION = "/home/dede/poppyNotes/code/src/poppynotes_dbusclient";
        const string PN_DESKTOP_FILE = "/home/dede/poppyNotes/code/data/poppynotes.desktop";
#endif
        const string GCONF_DIR_PN_SHOWHIDE = "/desktop/gnome/keybindings/poppynotes_showhide";
        const string GCONF_DIR_PN_SHOW     = "/desktop/gnome/keybindings/poppynotes_show";
        const string GCONF_DIR_PN_HIDE     = "/desktop/gnome/keybindings/poppynotes_hide";
        const string GCONF_DIR_PN_NEW      = "/desktop/gnome/keybindings/poppynotes_new";
        const string GCONF_DIR_PN_SETT     = "/desktop/gnome/keybindings/poppynotes_settings";

        private SettingsDialog _settings_dialog;
        
        public Settings(string file) {
            this.file = file;
        }

        private bool is_already_showing { get; set; default = false; }

        public Settings.with_properties(
                string file, string notes_directory, string default_content_type,
                bool start_with_system, bool show_on_startup,
                int default_width, int default_height,
                int default_pos_x, int default_pos_y,
                string default_font_family, float default_font_size,
                string default_font_color, string default_bg_color,
                ShortcutKey shortcut_showhide,
                ShortcutKey shortcut_show, ShortcutKey shortcut_hide,
                ShortcutKey shortcut_new, ShortcutKey shortcut_sett) {
            this.file = file;
            this.notes_directory = notes_directory;
            this.default_content_type = default_content_type;
            this.start_with_system = start_with_system;
            this.show_on_startup = show_on_startup;
            if (default_width > 0 && default_width < 1000)
                this.default_width = default_width;
            if (default_height > 0 && default_height < 1000)
                this.default_height = default_height;
            if (default_pos_x > 0 && default_pos_x < 1000)
                this.default_pos_x = default_pos_x;
            if (default_pos_y > 0 && default_pos_y < 1000)
                this.default_pos_y = default_pos_y;
            this.default_font_family = default_font_family;
            if (default_font_size > 0 && default_font_size < 50)
                this.default_font_size = default_font_size;
            this.default_font_color = default_font_color;
            this.default_bg_color = default_bg_color;
            this.shortcut_showhide = shortcut_showhide;
            this.shortcut_show = shortcut_show;
            this.shortcut_hide = shortcut_hide;
            this.shortcut_new = shortcut_new;
            this.shortcut_sett = shortcut_sett;
        }

        public string file { get; private set; }
        
        public string notes_directory { get; private set; default = "~/.poppynotes"; }
        public string default_content_type { get; private set; default = "plain"; }
        public bool start_with_system { get; private set; default = false; }
        public bool show_on_startup { get; private set; default = false; }
		public int default_width  { get; private set; default = 200; }
		public int default_height { get; private set; default = 200; }
		public int default_pos_x  { get; private set; default = 100; }
		public int default_pos_y  { get; private set; default = 100; }
        public string default_font_family { get; private set; default = "monospace"; }
        public float default_font_size { get; private set; default = 10.0f; }
        public string default_font_color { get; private set; default = "black"; }
        public string default_bg_color { get; private set; default = "white"; }
        public ShortcutKey shortcut_showhide { get; private set; }
        public ShortcutKey shortcut_show { get; private set; }
        public ShortcutKey shortcut_hide { get; private set; }
        public ShortcutKey shortcut_new { get; private set; }
        public ShortcutKey shortcut_sett { get; private set; }

        /*
         * Show a dialog to let the user edit the settings.
         */
        public void show_edit_dialog() {
            if (!this.is_already_showing) {
		        try {
			        var builder = new Builder();
			        builder.add_from_file(SETTINGS_UI_FILE);
			        var win = builder.get_object("settings_dialog") as SettingsDialog;
			        //builder.connect_signals(this);
                    win.delete_event.connect(() => {
				        this.is_already_showing = false; this.cancel(); return false;
                    });
                    win.init();
                    win.button_cancel.clicked.connect(this.cancel);
                    win.button_save.clicked.connect(this.update_save);
                    win.set_all_settings(this);
                    this._settings_dialog = win;
                    win.show_all();
                    this.is_already_showing = true;
		        } catch (GLib.Error e) {
			        stderr.printf("Could not load UI: %s\n", e.message);
		        }
            } else {
                this._settings_dialog.present();
            }
        }

        /*
         * Save the settings.
         */
        public void save() {
			var g = new Json.Generator();
			g.pretty = true;
			g.root = this.construct_json_node();
			try {
				g.to_file(this.file);
			} catch (GLib.Error e) {
				stderr.printf("Error: %s\n", e.message);
			}
            this.set_system_shortcut_showhide();
            this.set_system_shortcut_show();
            this.set_system_shortcut_hide();
            this.set_system_shortcut_new();
            this.set_system_shortcut_sett();
            this.set_system_autostart(this.start_with_system);
        }

/////////////////////////
//  Protected methods  //
/////////////////////////
        
        /*
         * Construct the Json.Node object from the settings.
         */
		protected Json.Node construct_json_node() {
			var p = new Json.Builder();
			p.begin_object();
				p.set_member_name("poppynotes_object_type");
				p.add_string_value("settings");
				p.set_member_name("version");
				p.add_string_value("1.0");
				p.set_member_name("notes_directory");
				p.add_string_value(this.notes_directory);
				p.set_member_name("default_content_type");
				p.add_string_value(this.default_content_type);
            	p.set_member_name("start_with_system");
				p.add_boolean_value(this.start_with_system);
            	p.set_member_name("show_on_startup");
				p.add_boolean_value(this.show_on_startup);
                p.set_member_name("default_width");
				p.add_int_value((int64) this.default_width);
				p.set_member_name("default_height");
				p.add_int_value((int64) this.default_height);
				p.set_member_name("default_pos_x");
				p.add_int_value((int64) this.default_pos_x);
				p.set_member_name("default_pos_y");
				p.add_int_value((int64) this.default_pos_y);
				p.set_member_name("default_font_family");
				p.add_string_value(this.default_font_family);
				p.set_member_name("default_font_size");
				p.add_double_value((double) this.default_font_size);
				p.set_member_name("default_font_color");
				p.add_string_value(this.default_font_color);
				p.set_member_name("default_bg_color");
				p.add_string_value(this.default_bg_color);
				p.set_member_name("shortcut_showhide");
				p.add_string_value(this.shortcut_showhide.to_string());
                p.set_member_name("shortcut_showall");
				p.add_string_value(this.shortcut_show.to_string());
                p.set_member_name("shortcut_hideall");
				p.add_string_value(this.shortcut_hide.to_string());
                p.set_member_name("shortcut_newnote");
				p.add_string_value(this.shortcut_new.to_string());
                p.set_member_name("shortcut_settings");
				p.add_string_value(this.shortcut_sett.to_string());
			p.end_object();
			return p.get_root();
		}

        /*
         * Cancel the settings edit.
         */
        protected void cancel() {
            // nothing to do here
            if (this._settings_dialog != null)
                this._settings_dialog.destroy();
            this.is_already_showing = false;
        }

        /*
         * Update the settings, i.e. get the edited settings from the dialog,
         * and save the settings (to file).
         */
        protected void update_save() {
            if (this._settings_dialog != null) {
                if (this.notes_directory != this._settings_dialog.notes_directory) {
                    this.notes_directory = this._settings_dialog.notes_directory;
                    // show info on the delay of this change taking effect
                    var d = new Gtk.MessageDialog(this._settings_dialog, Gtk.DialogFlags.MODAL, Gtk.MessageType.INFO, Gtk.ButtonsType.OK,
                                                  _("The change of the notes_directory will take effect the next time PoppyNotes is started."));
                    d.title = _("Information");
                    d.response.connect((response_id) => { d.destroy(); });
                    d.show();
                }
                this.default_content_type = this._settings_dialog.default_content_type;
                this.start_with_system = this._settings_dialog.start_with_system;
                this.show_on_startup = this._settings_dialog.show_on_startup;
                this.default_width = this.check_width(this._settings_dialog.default_width_pn);
                this.default_height = this.check_height(this._settings_dialog.default_height_pn);
                this.default_pos_x = this.check_pos_x(this._settings_dialog.default_pos_x);
                this.default_pos_y = this.check_pos_y(this._settings_dialog.default_pos_y);
                this.default_font_family = this._settings_dialog.default_font_family;
                this.default_font_size = this._settings_dialog.default_font_size;
                this.default_font_color = this._settings_dialog.default_font_color;
                this.default_bg_color = this._settings_dialog.default_bg_color;
                this.shortcut_showhide = this.check_shortcut_key(this._settings_dialog.shortcut_showhide);
                this.shortcut_show = this.check_shortcut_key(this._settings_dialog.shortcut_show);
                this.shortcut_hide = this.check_shortcut_key(this._settings_dialog.shortcut_hide);
                this.shortcut_new = this.check_shortcut_key(this._settings_dialog.shortcut_new);
                this.shortcut_sett = this.check_shortcut_key(this._settings_dialog.shortcut_sett);
                this._settings_dialog.destroy();
            }
            this.save();
            if (this._settings_dialog != null)
                this._settings_dialog.destroy();
            this.is_already_showing = false;
        }

        protected void set_system_shortcut_showhide() {
            this.set_gconf_shortcut(this.shortcut_showhide.enabled, GCONF_DIR_PN_SHOWHIDE,
                                    PN_DBUS_APPLICATION + " " + DBusUtil.DBUS_ACTION_SHOWHIDE,
                                    "PoppyNotes (Show/Hide)", this.shortcut_showhide.to_string());
        }
        protected void set_system_shortcut_show() {
            this.set_gconf_shortcut(this.shortcut_show.enabled, GCONF_DIR_PN_SHOW,
                                    PN_DBUS_APPLICATION + " " + DBusUtil.DBUS_ACTION_SHOW,
                                    "PoppyNotes (Show all)", this.shortcut_show.to_string());
        }
        protected void set_system_shortcut_hide() {
            this.set_gconf_shortcut(this.shortcut_hide.enabled, GCONF_DIR_PN_HIDE,
                                    PN_DBUS_APPLICATION + " " + DBusUtil.DBUS_ACTION_HIDE,
                                    "PoppyNotes (Hide all)", this.shortcut_hide.to_string());
        }
        protected void set_system_shortcut_new() {
            this.set_gconf_shortcut(this.shortcut_new.enabled, GCONF_DIR_PN_NEW,
                                    PN_DBUS_APPLICATION + " " + DBusUtil.DBUS_ACTION_NEWNOTE,
                                    "PoppyNotes (New note)", this.shortcut_new.to_string());
        }
        protected void set_system_shortcut_sett() {
            this.set_gconf_shortcut(this.shortcut_sett.enabled, GCONF_DIR_PN_SETT,
                                    PN_DBUS_APPLICATION + " " + DBusUtil.DBUS_ACTION_SETTINGS,
                                    "PoppyNotes (Settings)", this.shortcut_sett.to_string());
        }

        /*
         * If start_with_system:  create link to poppynotes.desktop in ~/.config/autostart 
         * If !start_with_system: remove link in ~/.config/autostart
         */
        protected void set_system_autostart(bool start_with_system) {
            string autostart_dir = Environment.get_home_dir() + "/.config/autostart";
            var autostart_file = File.new_for_path(autostart_dir + "/poppynotes-autostart.desktop");
            if (start_with_system && !autostart_file.query_exists()) {
                try {
                    autostart_file.make_symbolic_link(PN_DESKTOP_FILE);
                } catch (GLib.Error e) {
                    stderr.printf("Could not create symbolic link (autostart file)!\n");
                }
            } else if (autostart_file.query_exists()) {
                autostart_file.delete();
            }
        }
        
        /*
         * Set the binding to a shortcut key for show/hide in GConf.
         * (Note: GConf is deprecated, the new system to use is GSettings (which uses dconf) [GLib.Settings].
         *  in dconf there is the schema org.gnome.settings-daemon.plugins.media-keys which may
         *  contain a key custom-keybindings (a list), which stores paths to keys that define a
         *  custom keybinding (over properties name, binding, command).
         *  in Ubuntu 12.04 there is no key custom-keybindings by default, and it seems that the
         *  system does not support this mechanism yet.)
         */
        protected void set_gconf_shortcut(bool enabled, string path,
                                            string command, string name, string binding) {
            if (enabled) {
                try {
                    var cl = GConf.Client.get_default();
                    if (! cl.dir_exists(path)) {
                        cl.add_dir(path, GConf.ClientPreloadType.NONE);
                    }
                    cl.set_string(path + "/name", name);
                    cl.set_string(path + "/action", command);
                    cl.set_string(path + "/binding", binding);
                } catch (GLib.Error e) { stderr.printf(e.message); }
            } else {
                try {
                    var cl = GConf.Client.get_default();
                    if (cl.dir_exists(path)) {
                        cl.set_string(path + "/binding", "");
                        //TODO remove key completely! This does not work:
                        // GConf-WARNING **: Directory [..] was not being monitored by GConfClient
                        //cl.remove_dir(path);
                    }
                } catch (GLib.Error e) { stderr.printf(e.message); }
            }
        }

////////////////////////
//  Check user input  //
////////////////////////

        protected ShortcutKey check_shortcut_key(ShortcutKey key) {
            if (key.enabled && !key.charnum.isalpha()) {
                var d = new Gtk.MessageDialog(this._settings_dialog, Gtk.DialogFlags.MODAL, Gtk.MessageType.ERROR, Gtk.ButtonsType.OK,
                                              _("The key of shortcuts has to be a character between 'a' and 'z'."));
                d.title = _("Error");
                d.response.connect((response_id) => { d.destroy(); });
                d.show();
                key.charnum = 'n';
            }
            return key;
        }
        protected int check_width(int width) {
            if (width < 20 || width > 1000) {
                var d = new Gtk.MessageDialog(this._settings_dialog, Gtk.DialogFlags.MODAL, Gtk.MessageType.ERROR, Gtk.ButtonsType.OK,
                                              _("The default width has to be between 20px and 1000px."));
                d.title = _("Error");
                d.response.connect((response_id) => { d.destroy(); });
                d.show();
                width = 200;
            }
            return width;
        }
        protected int check_height(int height) {
            if (height < 20 || height > 1000) {
                var d = new Gtk.MessageDialog(this._settings_dialog, Gtk.DialogFlags.MODAL, Gtk.MessageType.ERROR, Gtk.ButtonsType.OK,
                                              _("The default height has to be between 20px and 1000px."));
                d.title = _("Error");
                d.response.connect((response_id) => { d.destroy(); });
                d.show();
                height = 200;
            }
            return height;
        }
        protected int check_pos_x(int pos_x) {
            if (pos_x < 0) {
                var d = new Gtk.MessageDialog(this._settings_dialog, Gtk.DialogFlags.MODAL, Gtk.MessageType.ERROR, Gtk.ButtonsType.OK,
                                              _("The default position X has to be greater than or equal to 0."));
                d.title = _("Error");
                d.response.connect((response_id) => { d.destroy(); });
                d.show();
                pos_x = 100;
            }
            return pos_x;
        }
        protected int check_pos_y(int pos_y) {
            if (pos_y < 0) {
                var d = new Gtk.MessageDialog(this._settings_dialog, Gtk.DialogFlags.MODAL, Gtk.MessageType.ERROR, Gtk.ButtonsType.OK,
                                              _("The default position Y has to be greater than or equal to 0."));
                d.title = _("Error");
                d.response.connect((response_id) => { d.destroy(); });
                d.show();
                pos_y = 100;
            }
            return pos_y;
        }
    }
}
