/* poppynotes@perichron.org +++ 2013-06-25
 * Class for loading settings.
 * Uses a contant settings file which cannot be changed at runtime!
 */
using Json;

namespace PoppyNotes {
    public class SettingsLoader : GLib.Object, ISettingsLoader {

		public const string settings_version = "1.0";
#if ( DEPLOY )
        public const string settings_file = Config.PACKAGE_DATA_DIR + "/" + "poppynotes.settings";
#else
        public const string settings_file = "data" + "/" + "poppynotes.settings";
#endif

        public Settings load_settings() {
			var p = new Json.Parser();
			try {
				p.load_from_file(settings_file);
				Json.Node r = p.get_root();
				Json.Object settings = r.get_object();
				return this.load_for_version(settings);
			} catch (GLib.Error e) {
				stderr.printf("Error: %s\n", e.message);
				stderr.printf(@"Error: Cannot read settings file! ($settings_file)\n");
                return new Settings(settings_file);
			}
        }

		protected Settings load_for_version(Json.Object settings) {
			string version = settings.get_string_member("version");
			if (version != settings_version) {
				stderr.printf(@"Settings file version should be \"$settings_version\" but is \"$version\" instead. \n" +
					"(I try to retrieve some settings anyway.)\n");
			}
			//anyway, try to load the settings
			switch (version) {
                case "1.0":
				default:
					return this.extract_settings_v1_0(settings);
			}
		}

		protected Settings extract_settings_v1_0(Json.Object settings) {
			string notes_dir = settings.get_string_member("notes_directory");
            string content_type = settings.get_string_member("default_content_type");
            bool start_with_system = settings.get_boolean_member("start_with_system");
            bool show_on_startup = settings.get_boolean_member("show_on_startup");
			int width = (int) settings.get_int_member("default_width");
			int height = (int) settings.get_int_member("default_height");
			int pos_x = (int) settings.get_int_member("default_pos_x");
			int pos_y = (int) settings.get_int_member("default_pos_y");
            string font_family = settings.get_string_member("default_font_family");
            float font_size = (float) settings.get_double_member("default_font_size");
            string font_color = settings.get_string_member("default_font_color");
			string bg_color = settings.get_string_member("default_bg_color");
            ShortcutKey shortcut_showhide = ShortcutKey.from_string(settings.get_string_member("shortcut_showhide"));
            ShortcutKey shortcut_show = ShortcutKey.from_string(settings.get_string_member("shortcut_showall"));
            ShortcutKey shortcut_hide = ShortcutKey.from_string(settings.get_string_member("shortcut_hideall"));
            ShortcutKey shortcut_new = ShortcutKey.from_string(settings.get_string_member("shortcut_newnote"));
            ShortcutKey shortcut_sett = ShortcutKey.from_string(settings.get_string_member("shortcut_settings"));
            return new Settings.with_properties(settings_file, notes_dir, content_type,
                                start_with_system, show_on_startup,
                                width, height, pos_x, pos_y,
                                font_family, font_size, font_color, bg_color,
                                shortcut_showhide,
                                shortcut_show, shortcut_hide,
                                shortcut_new, shortcut_sett);
		}
    }
}